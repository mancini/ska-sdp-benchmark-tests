SKA SDP Benchmark tests
========================

In order to add new test, follow the steps below

- Decide upon which system partitions the test should run. Add them to the test.
- Create an environment for test and include it in the system partitions where the test is meant to run. Also, include this environment in the `reframe_config.py` file.
- Add a docstring to the test file giving the instructions on how to run the test.
- Add more detailed documentation in the `docs/` folder.
- Create a Jupyter notebook to plot and tabulate the results of test in the test folder in `apps/`.
- Finally, run the tests on different systems and generate performance metrics.
- Push these generated performance logs in `perflogs/` directory to GitLab.

It is recommended to create a new branch out of main while adding new tests. More details on how to name branches and write commit messages can be found at [SKA developer portal](https://developer.skao.int/en/latest/tools/git.html#) guidelines.

Contact
----------

Please contact Mahendra Paipuri (mahendra.paipuri@inria.fr) to report bugs and for help.
