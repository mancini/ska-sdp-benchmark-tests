#!/bin/bash

###########################################################################
#
# This is part of SKA SDP Benchmark tests repository to set up the
# environment necessary to run the tests. This is purely optional step to
# reduce the typing on the command line and to have reframe on PATH through
# a bash function. It is possible to run the tests without setting up this
# environment.
#
# Source the script as follows:
#
#     . /path/to/ska-sdp-benchmark-tests/share/setup-env.sh
#
# This script has been inspired from Spack environment script
# and several bash functions are taken from it
#
###########################################################################

RFM_VERSION="4.6.1"

_pathadd() {
    # If no variable name is supplied, just append to PATH
    # otherwise append to that variable.
    _pa_varname=PATH
    _pa_new_path="$1"
    if [ -n "$2" ]; then
        _pa_varname="$1"
        _pa_new_path="$2"
    fi

    # Do the actual prepending here.
    eval "_pa_oldvalue=\${${_pa_varname}:-}"

    _pa_canonical=":$_pa_oldvalue:"
    if [ -d "$_pa_new_path" ] && \
       [ "${_pa_canonical#*:${_pa_new_path}:}" = "${_pa_canonical}" ];
    then
        if [ -n "$_pa_oldvalue" ]; then
            eval "export $_pa_varname=\"$_pa_new_path:$_pa_oldvalue\""
        else
            export $_pa_varname="$_pa_new_path"
        fi
    fi
}

# Determine which shell is being used
_determine_shell() {
    if [ -f "/proc/$$/exe" ]; then
        # If procfs is present this seems a more reliable
        # way to detect the current shell
        _sp_exe=$(readlink /proc/$$/exe)
        # Shell may contain number, like zsh5 instead of zsh
        basename ${_sp_exe} | tr -d '0123456789'
    elif [ -n "${BASH:-}" ]; then
        echo bash
    elif [ -n "${ZSH_NAME:-}" ]; then
        echo zsh
    else
        PS_FORMAT= ps -p $$ | tail -n 1 | awk '{print $4}' | sed 's/^-//' | xargs basename
    fi
}

_python=`which python3`

# We need to exit with a zero code if the Python version is the correct
# one, so we invert the comparison
if $_python -c 'import sys; sys.exit(sys.version_info[:2] >= (3, 6))'; then
    echo -e "SDP Benchmarks requires Python >= 3.6 (found $($_python -V 2>&1))"
    exit 1
fi

# Get current shell type
_current_shell=$(_determine_shell)

# Get current working directory
_current_work_dir=$PWD

# Get the location of file
if [ "$_current_shell" = bash ]; then
    _source_file="${BASH_SOURCE[0]:-}"
elif [ "$_current_shell" = zsh ]; then
    _source_file="${(%):-%N}"
else
    # Try to read the /proc filesystem (works on linux without lsof)
    # In dash, the sourced file is the last one opened (and it's kept open)
    _source_file_fd="$(\ls /proc/$$/fd 2>/dev/null | sort -n | tail -1)"
    if ! _source_file="$(readlink /proc/$$/fd/$_source_file_fd)"; then
        # Last resort: try lsof. This works in dash on macos -- same reason.
        # macos has lsof installed by default; some linux containers don't.
        _lsof_output="$(lsof -p $$ -Fn0 | tail -1)"
        _source_file="${_lsof_output#*n}"
    fi

    # If we can't find this script's path after all that, bail out with
    # plain old $0, which WILL NOT work if this is sourced indirectly.
    if [ ! -f "$_sp_source_file" ]; then
        _source_file="$0"
    fi
fi

#
# Find root directory
#
# We send cd output to /dev/null to avoid because a lot of users set up
# their shell so that cd prints things out to the tty.
if [ "$_current_shell" = zsh ]; then
    _share_dir="${_source_file:A:h}"
    _prefix="${_share_dir:h}"
else
    _share_dir="$(cd "$(dirname $_source_file)" > /dev/null && pwd)"
    _prefix="$(cd "$(dirname $_share_dir)" > /dev/null && pwd)"
fi

#
# Install pip dependencies
#
# Disable the user installation scheme which is the default for Debian and
# cannot be combined with `--target`
export PIP_USER=0

_pip_install_dir="$_prefix/external"
_benchmon_install_dir="$_pip_install_dir/benchmon"

# Install dependencies in external/ folder
# We assume if folder external/ exists, pip dependencies are already installed
if [ ! -d "$_pip_install_dir" ]; then
  echo "Installing Dependencies"
  $_python -m pip install --no-cache-dir -q -r $_prefix/requirements.txt --target=$_pip_install_dir --upgrade
fi

install_benchmon=${INSTALL_BENCHMON:-yes}
if [ ! -d "$_benchmon_install_dir" ] && [ "$install_benchmon" == 'yes' ]; then
  echo "Installing benchmon"
  $_python -m pip install --no-cache-dir -q git+https://gitlab.com/ska-telescope/sdp/ska-sdp-benchmark-monitor.git --target="$_benchmon_install_dir" --upgrade
fi

# Add external/ to PYTHONPATH
_pathadd PYTHONPATH "$_pip_install_dir"
_pathadd PYTHONPATH "$_benchmon_install_dir"

# Add external/bin to PATH
_pathadd PATH "$_pip_install_dir/bin"
_pathadd PATH "$_benchmon_install_dir/bin"
#
# Fetch ReFrame only if repository is not found
#
_reframe_dir="$_prefix/reframe"
if [ -d "$_reframe_dir" ]; then
  rfm_version=$( "$_reframe_dir/bin/reframe" --version )
  if [ "$rfm_version" != "$RFM_VERSION" ]; then
    echo "Removing old ReFrame version $rfm_version"
    rm -rf "$_reframe_dir"
  fi
fi

if [ ! -d "$_reframe_dir" ]; then
  echo "Cloning ReFrame"
  # Fetch ReFrame source code from GitHub
  git clone --depth 1 --branch "v$RFM_VERSION" https://github.com/eth-cscs/reframe.git $_reframe_dir > /dev/null 2>&1

  # We need to boostrap ReFrame to install its dependencies
  cd $_reframe_dir && ./bootstrap.sh > /dev/null 2>&1 && cd $_current_work_dir
fi

# Add ReFrame dependencies to PYTHONPATH
_pathadd PYTHONPATH "$_prefix/reframe"
_pathadd PATH "$_prefix/reframe/bin"

# Add ReFrame config file to environment variable
export RFM_CONFIG_FILES="$_prefix/reframe_config.py"

# Set an environment variable that points to root of the repository
export SDPTESTS_ROOT="$_prefix"

# Mark Benchmon-Tests to run
export USE_BENCHMON="YES"

# Define the reframe shell function
eval "reframe() {
    : this is a shell function from: $_share_dir/setup-env.sh
    : the real reframe script is here: $_prefix/reframe/bin/reframe
    ${_prefix}/reframe/bin/reframe \"\$@\"
    return \$?
}"
