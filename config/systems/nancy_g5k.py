""""This file contains the configuration of nancy grid5000 and their partitions"""

import os
from pathlib import Path
import json

# Get root dir
root_dir = Path(__file__).parent.parent.parent

# Load topology files
grisou_topo = json.load(
    open(os.path.join(root_dir, 'topologies', 'grisou-grid5000.json'), 'r')
)
gros_topo = json.load(
    open(os.path.join(root_dir, 'topologies', 'gros-grid5000.json'), 'r')
)
grouille_topo = json.load(
    open(os.path.join(root_dir, 'topologies', 'grouille-grid5000.json'), 'r')
)


nancy_g5k_system_config = {
    'name': 'nancy-g5k',
    'descr': 'Grid5000 is a large-scale and flexible testbed for experiment-driven '
             'research: https://www.grid5000.fr/w/Grid5000:Home.This '
             'is Nancy partition: https://www.grid5000.fr/w/Nancy:Hardware',
    'hostnames': ['fnancy', 'grisou-*'],
    'modules_system': 'lmod',
    'partitions': [
        {
            'name': 'login',
            'descr': 'Generic frontend nodes for nancy clusters',
            'scheduler': 'local',
            'launcher': 'local',
            'environs': [
                'builtin', 'gnu',
            ],
            'prepare_cmds': [
                'module purge',
            ],
        },
        {
            'name': 'gros-gcc9-ompi4-eth-smod',
            'descr': 'Gros cluster at the nancy site with 2x25 Gbps , '
                     'Ethernet and generic configuration provided by g5k '
                     '(Intel Xeon Gold 5220) '
                     'https://www.grid5000.fr/w/Nancy:Hardware#gros',
            'scheduler': 'oar',
            'launcher': 'mpirun',
            'time_limit': '0d8h0m0s',
            'access': [
                '-p cluster=\'gros\'',
            ],
            'environs': [
                'builtin',
            ],
            'env_vars': [
                # specific for g5000 clusters
                ['SCRATCH_DIR', '/tmp/'],
            ],
            'processor': {
                **gros_topo,
            },
            'prepare_cmds': [  # oar specific commands
                'source /etc/profile.d/lmod.sh',
                'source $HOME/.bashrc',
                'module purge',
                'mpirun () { command $(which mpirun) --hostfile $OAR_NODEFILE --mca orte_rsh_agent \"oarsh\" '
                '\"$@\"; }',  # wrap mpirun with hostfile arg
            ],
            'extras': {
                'interconnect': '10',  # in Gb/s
                'mem': '96000000000',  # total memory in bytes
            },
        },
        {
            'name': 'gros-gcc9-ompi4-eth-umod',
            'descr': 'Gros cluster at the nancy site with 2x25 Gbps , '
                     'Ethernet and with GCC 9.3.0 and OpenMPI 4.1.1 with UCX '
                     '(Intel Xeon Gold 5220) '
                     'https://www.grid5000.fr/w/Nancy:Hardware#gros',
            'scheduler': 'oar',
            'launcher': 'mpirun',
            'time_limit': '0d8h0m0s',
            'access': [
                '-p cluster=\'gros\'',
            ],
            'environs': [
                'babel-stream-omp',
                'builtin',
                'imaging-iotest',
                'imaging-iotest-mkl',
                'imb',
                'numpy',
                'rascil',
            ],
            'env_vars': [
                # specific for g5000 clusters
                ['SCRATCH_DIR', '/tmp/'],
            ],
            'modules': [
                'gcc/9.3.0', 'openmpi/4.1.1',
            ],
            'processor': {
                **gros_topo,
            },
            'prepare_cmds': [  # oar specific commands
                'source /etc/profile.d/lmod.sh',
                'source $HOME/.bashrc',
                'module purge',
                'module use ${SPACK_ROOT}/var/spack/environments/nancy-g5k-gros/lmod/linux*/Core',
                'mpirun () { command $(which mpirun) --hostfile $OAR_NODEFILE --mca orte_rsh_agent \"oarsh\" '
                '\"$@\"; }',  # wrap mpirun with hostfile arg
            ],
            'extras': {
                'interconnect': '10',  # in Gb/s
                'mem': '96000000000',  # total memory in bytes
            },
        },
        {
            'name': 'gros-icc21-impi21-eth-umod',
            'descr': 'Gros cluster at the nancy site with 2x25 Gbps , '
                     'Ethernet and ICC 2021.4.0 and Intel MPI 2021.4.0'
                     '(Intel Xeon Gold 5220) '
                     'https://www.grid5000.fr/w/Nancy:Hardware#gros',
            'scheduler': 'oar',
            'launcher': 'mpiexec',
            'time_limit': '0d8h0m0s',
            'access': [
                '-p cluster=\'gros\'',
            ],
            'environs': [
                'babel-stream-tbb',
                'builtin',
                'intel-hpcg',
                'intel-hpl',
                'imaging-iotest',
                'imaging-iotest-mkl',
                'intel-stream',
            ],
            'env_vars': [
                # specific for g5000 clusters
                ['SCRATCH_DIR', '/tmp/'],
            ],
            'modules': [
                'intel-oneapi-compilers/2021.4.0',
                'intel-oneapi-mpi/2021.4.0',
            ],
            'processor': {
                **gros_topo,
            },
            'prepare_cmds': [  # oar specific commands
                'source /etc/profile.d/lmod.sh',
                'source $HOME/.bashrc',
                'module purge',
                'module use ${SPACK_ROOT}/var/spack/environments/nancy-g5k-gros/lmod/linux*/Core',
                'mpiexec () { command $(which mpiexec) -genvall -f $OAR_NODEFILE '
                '-launcher ssh -launcher-exec /usr/bin/oarsh \"$@\"; }',  # wrapping mpirun with hostfile arg
            ],
            'extras': {
                'interconnect': '10',  # in Gb/s
                'mem': '96000000000',  # total memory in bytes
            },
        },  # <end gros partition>
        {
            'name': 'grouille-gcc9-ompi4-eth-smod-nvgpu',
            'descr': 'Grouille cluster at the nancy site with 25 Gbps , '
                     'Ethernet, 2 x Nvidia A100 (40 GiB) and generic '
                     'configuration provided by g5k '
                     '(2 x AMD EPYC 7452) '
                     'https://www.grid5000.fr/w/Nancy:Hardware#grouille',
            'scheduler': 'oar',
            'launcher': 'mpirun',
            'time_limit': '0d1h0m0s',
            'access': [
                '-p cluster=\'grouille\'',
                '-t exotic',
            ],
            'environs': [
                'builtin',
            ],
            'env_vars': [
                # specific for g5000 clusters
                ['SCRATCH_DIR', '/tmp/'],
            ],
            'processor': {
                **grouille_topo,
            },
            'devices': [
                {
                    'type': 'gpu',
                    'arch': '80',
                    'num_devices': 2,
                },
            ],
            'prepare_cmds': [  # oar specific commands
                'source /etc/profile.d/lmod.sh',
                'source $HOME/.bashrc',
                'module purge',
                'mpirun () { command $(which mpirun) --hostfile $OAR_NODEFILE --mca orte_rsh_agent \"oarsh\" '
                '\"$@\"; }',  # wrap mpirun with hostfile arg
            ],
            'extras': {
                'interconnect': '25',  # in Gb/s
                'mem': '128000000000',  # total memory in bytes
                'gpu_mem': '42505076736',  # in bytes
            },
        },
        {
            'name': 'grouille-gcc9-ompi4-eth-umod-nvgpu',
            'descr': 'Grouille cluster at the nancy site with 25 Gbps , '
                     'Ethernet, 2 x Nvidia A100 (40 GiB) and OpenMPI 4.1.1 '
                     'with UCX support (2 x AMD EPYC 7452) '
                     'https://www.grid5000.fr/w/Nancy:Hardware#grouille',
            'scheduler': 'oar',
            'launcher': 'mpirun',
            'time_limit': '0d1h0m0s',
            'access': [
                '-p cluster=\'grouille\'',
                '-t exotic',
            ],
            'environs': [
                'babel-stream-cuda',
                'builtin',
                'idg-test',
                'nccl-test',
            ],
            'env_vars': [
                # specific for g5000 clusters
                ['SCRATCH_DIR', '/tmp/'],
                # Access Control System (ACS) is enabled on this machine
                # GPU P2P comms are disabled due to ACS and perf tests will
                # fail to communicate between peers. We disable P2P for this system
                # to use shared memory
                ['NCCL_P2P_DISABLE', '1'],
            ],
            'modules': [
                'gcc/9.3.0', 'openmpi/4.1.1-cuda-11.2.0',
            ],
            'processor': {
                **grouille_topo,
            },
            'devices': [
                {
                    'type': 'gpu',
                    'arch': '80',
                    'num_devices': 2,
                },
            ],
            'prepare_cmds': [  # oar specific commands
                'source /etc/profile.d/lmod.sh',
                'source $HOME/.bashrc',
                'module purge',
                'module use ${SPACK_ROOT}/var/spack/environments/nancy-g5k-grouille/lmod/linux*/Core',
                'mpirun () { command $(which mpirun) --hostfile $OAR_NODEFILE --mca orte_rsh_agent \"oarsh\" '
                '\"$@\"; }',  # wrap mpirun with hostfile arg
            ],
            'extras': {
                'interconnect': '25',  # in Gb/s
                'mem': '128000000000',  # total memory in bytes
                'gpu_mem': '42505273344'  # in bytes
            },
        },  
        {
            'name': 'grouille-gcc9-ompi4-eth-umod',
            'descr': 'Grouille cluster at the nancy site with 25 Gbps , '
                     'Ethernet, WITHOUT GPUs and OpenMPI 4.1.1 '
                     'with UCX support (2 x AMD EPYC 7452) '
                     'https://www.grid5000.fr/w/Nancy:Hardware#grouille',
            'scheduler': 'oar',
            'launcher': 'mpirun',
            'time_limit': '0d1h0m0s',
            'access': [
                '-p cluster=\'grouille\'',
                '-t exotic',
            ],
            'environs': [
                'babel-stream-omp',
                'builtin',
            ],
            'env_vars': [
                # specific for g5000 clusters
                ['SCRATCH_DIR', '/tmp/'],
            ],
            'modules': [
                'gcc/9.3.0', 'openmpi/4.1.1-cuda-11.2.0',
            ],
            'processor': {
                **grouille_topo,
            },
            'prepare_cmds': [  # oar specific commands
                'source /etc/profile.d/lmod.sh',
                'source $HOME/.bashrc',
                'module purge',
                'module use ${SPACK_ROOT}/var/spack/environments/nancy-g5k-grouille/lmod/linux*/Core',
                'mpirun () { command $(which mpirun) --hostfile $OAR_NODEFILE --mca orte_rsh_agent \"oarsh\" '
                '\"$@\"; }',  # wrap mpirun with hostfile arg
            ],
            'extras': {
                'interconnect': '25',  # in Gb/s
                'mem': '128000000000',  # total memory in bytes
            },
        },  # <end grouille partition>
    ]
}
