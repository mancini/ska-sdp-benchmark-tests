""""This file contains the configuration of csd3 at Cambridge and its partitions"""

import os
from pathlib import Path
import json

# Get root dir
root_dir = Path(__file__).parent.parent.parent

# Load topology files
csd3_login_topo = json.load(
    open(os.path.join(root_dir, 'topologies', 'csd3-login-icelake.json'), 'r')
)
csd3_compute_topo = json.load(
    open(os.path.join(root_dir, 'topologies', 'csd3-compute-cclake.json'), 'r')
)
csd3_system_config = {
    'name': 'csd3',
    'descr': 'CSD3 cluster at Cambridge',
    'hostnames': ['login-q-*'],
    'modules_system': 'tmod4',
    'partitions': [
        {
            'name': 'login',
            'descr': 'Login node of CSD3 @ Cambridge',
            'scheduler': 'local',
            'launcher': 'local',
            'environs': [
                'builtin',
                'gnu',
            ],
            'processor': {
                **csd3_login_topo,
            },
            'max_jobs': 1,
            'modules': [
            ],
        },
        # cclake partition (https://docs.hpc.cam.ac.uk/hpc/user-guide/cclake.html)
        {
            'name': 'csd3-gcc9-cclake-cpu',
            'descr': 'cclake CPU partition',
            'scheduler': 'slurm',
            'launcher': 'srun',
            'time_limit': '0d12h0m0s',
            'access': [
                '--account=SKA-SDHP-SL2-CPU',
                '--partition=cclake',
                '--exclusive',
                # Overwrites ntasks and ntasks-per-node coming from the topology file)
                # Can be overwritten on a per-test basis
                '--nodes=1',
                '--ntasks=56',
                '--ntasks-per-node=56'
            ],
            # Max number of concurrent tests (ie. benchmarks for us) run by reframe
            'max_jobs': 8,
            'environs': [
                'builtin',
                'gnu',
                'numpy',
                #'rapthor',
                'imaging-iotest',
            ],
            'modules': [
                'gcc/9',
            ],
            'processor': {
                **csd3_compute_topo,
            },
            'prepare_cmds': [
                '. /etc/profile.d/modules.sh',   # Leave this line (enables the module command)
                'module purge',                  # Removes all modules still loaded
                #'module load rhel8/default-icl', # REQUIRED - loads the basic environment
                # 'export RAPTHOR_DIR= ', #TO COMPLETE
                # 'export INPUT_MS_PATH= ', #TO COMPLETE
                # 'source ', #TO COMPLETE: source setup-env.sh spack environment
            ],
            'extras': {
                #'mem': '206158430208' # total memory in bytes (per node)
            },
            'devices': [
                {
                },
            ],
            'env_vars': [
                #['SCRATCH_DIR', str(os.getenv('SCRATCH'))],
            ],
        },
    ]
}

""""This file contains the configuration of csd3 at Cambridge and its partitions"""
