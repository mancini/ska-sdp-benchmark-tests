""""This file contains the configuration of Marconi100 and their partitions"""

import os
from pathlib import Path
import json

# Get root dir
root_dir = Path(__file__).parent.parent.parent

# Load topology files
marconi_login_topo = json.load(
    open(os.path.join(root_dir, 'topologies', 'marconi100-login.json'), 'r')
)
marconi_compute_topo = json.load(
    open(os.path.join(root_dir, 'topologies', 'marconi100-compute.json'), 'r')
)


marconi100_system_config = {
    'name': 'marconi100',
    'descr': 'Marconi100 Cluster is hosted at CINECA, Italy with following config: '
             'https://wiki.u-gov.it/confluence/display/SCAIUS/UG3.2%3A+MARCONI100+UserGuide#UG3.2:MARCONI100UserGuide-SystemArchitecture',
    'hostnames': ['login*', 'r([0-9]+)n([0-9]+)'],
    'modules_system': 'tmod4',
    'partitions': [
        {
            'name': 'login',
            'descr': 'Login nodes for Marconi 100 Cluster',
            'scheduler': 'local',
            'launcher': 'local',
            'environs': ['builtin', 'gnu'],
            'processor': {
                **marconi_login_topo,
            },
            'prepare_cmds': [
                'module purge',
            ],
        },  # <end login node>
        {
            'name': 'prod-xl16-smpi10-ib-smod-nvgpu-small',
            'descr': 'This partition uses the IBM XL compiler 16.01 and spectrum mpi 10.4.0 '
                     'provided on Marconi100 cluster. This partition has max limit on number of '
                     'nodes of 16',
            'scheduler': 'slurm',
            'launcher': 'mpirun',
            'time_limit': '0d2h0m0s',
            'access': [
                '--partition=m100_usr_prod',
                '--account=Ppp4x_5680',
                '--qos=normal'
            ],
            'environs': [
                'builtin',
            ],
            'modules': [
                'xl/16.1.1--binary', 'git/2.27.0',
                'spectrum_mpi/10.4.0--binary',
            ],
            'env_vars': [
                ['SCRATCH_DIR', '/m100_scratch/userexternal/mpaipuri'],
                ['UCX_NET_DEVICES', 'mlx5_0:1'],
            ],
            'processor': {
                **marconi_compute_topo,
            },
            'devices': [
                {
                    'type': 'gpu',
                    'arch': '70',
                    'num_devices': 4,
                },
            ],
            'prepare_cmds': [
                'module purge',
                'module use ${SPACK_ROOT}/var/spack/environments/marconi100/modules/linux*',
                # 'mpirun () { command mpirun --tag-output --timestamp-output '
                # '\"$@\"; }',  # wrap mpirun output tag and timestamp
            ],
            'extras': {
                'interconnect': '100',  # in Gb/s
                'mem': '242000000000',  # total memory in bytes
                'gpu_mem': '16944988160',  # in bytes
                'hpcg_arch': 'p9',  # POWER9 architecture var needed for HPCG test
                'gdr_test_net_adptr': 'mlx5_0:1',  # NIC that has end-to-end connectivity for GDR test
            },
        },  # <end prod normal queue partition xl/spectrum mpi>
        {
            'name': 'prod-gcc9-ompi4-ib-umod-nvgpu-small',
            'descr': 'This partition uses the gcc 9.3.0 and openmpi 4.1.1 '
                     'built by Spack on Marconi100 cluster. This partition has max limit on number '
                     'of nodes of 16',
            'scheduler': 'slurm',
            'launcher': 'mpirun',
            'time_limit': '0d2h0m0s',
            'access': [
                '--partition=m100_usr_prod',
                '--account=Ppp4x_5680',
                '--qos=normal'
            ],
            'environs': [
                'builtin',
            ],
            'modules': [
                'gcc/9.3.0', 'git/2.27.0',
                'openmpi/4.1.1',
            ],
            'env_vars': [
                ['SCRATCH_DIR', '/m100_scratch/userexternal/mpaipuri'],
                ['UCX_NET_DEVICES', 'mlx5_0:1'],
            ],
            'processor': {
                **marconi_compute_topo,
            },
            'devices': [
                {
                    'type': 'gpu',
                    'arch': '70',
                    'num_devices': 4,
                },
            ],
            'prepare_cmds': [
                'module purge',
                'module use ${SPACK_ROOT}/var/spack/environments/marconi100/modules/linux*',
                # 'mpirun () { command mpirun --tag-output --timestamp-output '
                # '\"$@\"; }',  # wrap mpirun output tag and timestamp
            ],
            'extras': {
                'interconnect': '100',  # in Gb/s
                'mem': '242000000000',  # total memory in bytes
                'gpu_mem': '16944988160',  # in bytes
                'hpcg_arch': 'p9',  # POWER9 architecture var needed for HPCG test
                'gdr_test_net_adptr': 'mlx5_0:1',  # NIC that has end-to-end connectivity for GDR test
            },
        },  # <end prod normal queue partition gcc/open mpi>
        # <END NORMAL JOB QUEUE MARCONI100 - This queue has maximum of 16 nodes in the reservation>
        {
            'name': 'prod-xl16-smpi10-ib-smod-nvgpu-big',
            'descr': 'This partition uses the IBM XL compiler 16.01 and spectrum mpi 10.4.0 '
                     'provided on Marconi100 cluster. This partition has node limits from '
                     '17 to 256',
            'scheduler': 'slurm',
            'launcher': 'mpirun',
            'time_limit': '0d2h0m0s',
            'access': [
                '--partition=m100_usr_prod',
                '--account=Ppp4x_5680',
                '--qos=m100_qos_bprod'
            ],
            'environs': [
                'builtin',
            ],
            'modules': [
                'xl/16.1.1--binary', 'git/2.27.0',
                'spectrum_mpi/10.4.0--binary',
            ],
            'env_vars': [
                ['SCRATCH_DIR', '/m100_scratch/userexternal/mpaipuri'],
                ['UCX_NET_DEVICES', 'mlx5_0:1'],
            ],
            'processor': {
                **marconi_compute_topo,
            },
            'devices': [
                {
                    'type': 'gpu',
                    'arch': '70',
                    'num_devices': 4,
                },
            ],
            'prepare_cmds': [
                'module purge',
                'module use ${SPACK_ROOT}/var/spack/environments/marconi100/modules/linux*',
                # 'mpirun () { command mpirun --tag-output --timestamp-output '
                # '\"$@\"; }',  # wrap mpirun output tag and timestamp
            ],
            'extras': {
                'interconnect': '100',  # in Gb/s
                'mem': '242000000000',  # total memory in bytes
                'gpu_mem': '16944988160',  # in bytes
                'hpcg_arch': 'p9',  # POWER9 architecture var needed for HPCG test
                'gdr_test_net_adptr': 'mlx5_0:1',  # NIC that has end-to-end connectivity for GDR test
            },
        },  # <end prod big queue partition xl/spectrum mpi>
        {
            'name': 'prod-gcc9-ompi4-ib-umod-nvgpu-big',
            'descr': 'This partition uses the gcc 9.3.0 and openmpi 4.1.1 '
                     'built by Spack on Marconi100 cluster. This partition has node limits from '
                     '17 to 256',
            'scheduler': 'slurm',
            'launcher': 'mpirun',
            'time_limit': '0d2h0m0s',
            'access': [
                '--partition=m100_usr_prod',
                '--account=Ppp4x_5680',
                '--qos=m100_qos_bprod'
            ],
            'environs': [
                'builtin',
            ],
            'modules': [
                'gcc/9.3.0', 'git/2.27.0',
                'openmpi/4.1.1',
            ],
            'env_vars': [
                ['SCRATCH_DIR', '/m100_scratch/userexternal/mpaipuri'],
                ['UCX_NET_DEVICES', 'mlx5_0:1'],
            ],
            'processor': {
                **marconi_compute_topo,
            },
            'devices': [
                {
                    'type': 'gpu',
                    'arch': '70',
                    'num_devices': 4,
                },
            ],
            'prepare_cmds': [
                'module purge',
                'module use ${SPACK_ROOT}/var/spack/environments/marconi100/modules/linux*',
                # 'mpirun () { command mpirun --tag-output --timestamp-output '
                # '\"$@\"; }',  # wrap mpirun output tag and timestamp
            ],
            'extras': {
                'interconnect': '100',  # in Gb/s
                'mem': '242000000000',  # total memory in bytes
                'gpu_mem': '16944988160',  # in bytes
                'hpcg_arch': 'p9',  # POWER9 architecture var needed for HPCG test
                'gdr_test_net_adptr': 'mlx5_0:1',  # NIC that has end-to-end connectivity for GDR test
            },
        },  # <end prod big queue partition gcc/open mpi>
        # <END BIG JOB QUEUE MARCONI100 - This queue has maximum of 256 nodes in the reservation>
    ]
}
