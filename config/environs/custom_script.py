""""This file contains the environment config for babel stream benchmark"""

custom_script_environ = [
    {
        'name': 'custom_script',
        'cc': 'gcc',
        'cxx': 'g++',
        'ftn': 'gfortran',
        'modules': [],
        'target_systems': [
            'alaska:compute-gcc9-ompi4-roce-umod',
            'alaska:compute-icc21-impi21-roce-umod',
            # <end - alaska partitions>

            'grenoble-g5k:dahu-gcc9-ompi4-opa-umod',
            'grenoble-g5k:dahu-icc21-impi21-opa-umod',
            # <end - grenoble partitions>

            'juwels-booster:booster-gcc9-ompi4-ib-umod',
            'juwels-cluster:batch-icc21-impi21-ib-umod',
            'juwels-booster:booster-gcc9-ompi4-ib-umod-nvgpu',
            # <end - juwels partitions>

            'nancy-g5k:gros-gcc9-ompi4-eth-umod',
            'nancy-g5k:grouille-gcc9-ompi4-eth-umod',
            'nancy-g5k:gros-icc21-impi21-eth-umod',
            'nancy-g5k:grouille-gcc9-ompi4-eth-umod-nvgpu',
            # <end - nancy partitions>

            'cscs-daint:daint-gcc9-ompi4-ib-umod-gpu',
            # <end - cscs partitions>

            'licallo',
            # <end - licallo partitions>

            'jeanzay:jeanzay-gcc8-ompi4-opa-smod-cpu-short',
            'jeanzay:jeanzay-gcc8-ompi4-opa-smod-gpu',
            # <end - jeanzay partitions>
        ],
    },
]
