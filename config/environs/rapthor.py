""""This file contains the environment config for the Rapthor pipeline tests"""


rapthor_environ = [
    {
        'name': 'rapthor',
        'cc': 'gcc',
        'cxx': 'g++',
        'ftn': 'gfortran',
        'target_systems': [
            'jeanzay',
            'jeanzay:jeanzay-gcc8-ompi4-opa-smod-cpu-short',
            'jeanzay:jeanzay-gcc8-ompi4-opa-smod-cpu-long',
            # <end - jeanzay partitions>
        ],
    },
]
