""""This file contains the environment config for CUDA NIFTY gridder (CNG) tests"""


cng_test_environ = [
    {
        'name': 'cng-test',
        'cc': 'gcc',
        'cxx': 'g++',
        'ftn': 'gfortran',
        'modules': [
           'cuda/11.2.0',
        ],
        'target_systems': [
            'lyon-g5k:gemini-gcc9-ompi4-ib-umod-nvgpu',
            # <end - lyon partitions>
        ],
    },
    {
        'name': 'cng-test',
        'cc': 'gcc',
        'cxx': 'g++',
        'ftn': 'gfortran',
        'modules': [
           'cudatoolkit',
        ],
        'target_systems': [
            'cscs-daint:daint-gcc9-ompi4-ib-umod-gpu'
            # <end - cscs daint partitions>
        ],
    },
    {
        'name': 'cng-test',
        'cc': 'gcc',
        'cxx': 'g++',
        'ftn': 'gfortran',
        'env_vars': [
            ['LD_LIBRARY_PATH', '/usr/local/cuda-11.3/compat:/usr/local/cuda/lib64:$LD_LIBRARY_PATH'],
        ],
        'target_systems': [
            'gitlab-ci:k8-runner',
            # <end - Gitlab CI runner>
        ],
    },
]
