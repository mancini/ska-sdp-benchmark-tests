---
name: Merge Request

---

**Description of merge request**


**Interfaces**
Will this merge request result in a backwards-incompatible interface change:

Expected semantic version number increment category (Please indicate x.y.z):


**Issues this merge request solves**


**New tag of master**


**New deployment**


**Keep branch after merging**


**Notes**
