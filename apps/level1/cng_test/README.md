# CUDA NIFTY Gridder (CNG) Benchmark

This app contains CUDA NIFTY gridder benchmark test. More details on benchmark, instructions to run are detailed in the [documentation](https://developer.skao.int/projects/ska-sdp-benchmark-tests/en/latest/content/benchmarks.html#cuda-nifty-gridder-performance-benchmark). The source code of the benchmark is in `src/` folder.

The Jupyter notebook `CNG.ipynb` can be used to tabulate and plot predict and invert times that are extracted during benchmarking tests. The notebook must be invoked from the `ska-sdp-benchmark-tests` root directory.
