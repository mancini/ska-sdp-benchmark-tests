"""Set logger"""

import sys
import logging


def set_logger(config):
    """Set logger configuration"""

    if config['verbose']:
        log_level = logging.DEBUG
    else:
        log_level = logging.INFO

    # Enable logging
    logger = logging.getLogger()
    logger.setLevel(log_level)

    # Log format
    log_formatter = logging.Formatter(
        '%(asctime)s | %(levelname)s | %(threadName)s | %(name)s:%(funcName)s '
        '| %(lineno)d || %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S')

    # Handler to write logs to stdout
    stream_handler = logging.StreamHandler(sys.stdout)
    stream_handler.setFormatter(log_formatter)
    stream_handler.setLevel(log_level)

    # Select loggers
    logger.addHandler(stream_handler)
