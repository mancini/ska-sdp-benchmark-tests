#!/usr/bin/env python
"""Setup file for CNG benchmark"""

import os
import sys
import subprocess
from setuptools import setup


# Install pip package
def install(package, editable=False):
    """Install package using pip

    :param package: Name of the package to be installed
    :type package: str
    :param editable: Bool to indicate if package needs to be installed as editable
    :type editable: bool
    """

    cmd = [sys.executable, '-m', 'pip', 'install']
    if editable:
        cmd.extend(['--editable', package])
    else:
        cmd.extend([package])
    subprocess.check_call(cmd)


# numpy is build dependency for CUDA NIFTY gridder
#install('numpy')
#install('git+https://gitlab.com/ska-telescope/sdp/ska-gridder-nifty-cuda.git@master'
#        '#egg=ska-gridder-nifty-cuda', editable=True)

# Bail on Python < 3
assert sys.version_info[0] >= 3

# Get version information
version = {}
VERSION_PATH = os.path.join('cng_test', '_version.py')
with open(VERSION_PATH, 'r') as file:
    exec(file.read(), version)

# Load local readme file
with open('README.md') as readme_file:
    readme = readme_file.read()

# List of all packages
packages = [
    'cng_test',
]

# Package data
package_data = {
    'cng_test': [
        'cfgdata/*.cfg'
    ]
}

# List of all required packages
reqs = [
    line.strip() for line in open('requirements.txt').readlines()
]

# setup config
setup(
    name='cng-benchmark',
    version=version['__version__'],
    python_requires='>=3.7',
    description='A simple package to benchmark CUDA NIFTY gridder',
    long_description=readme + '\n\n',
    maintainer='Mahendra Paipuri',
    maintainer_email='mahendra.paipuri@inria.fr',
    url='https://gitlab.com/ska-telescope/sdp/ska-sdp-benchmark-tests',
    project_urls={
        'Documentation': 'https://gitlab.com/ska-telescope/sdp/ska-sdp-benchmark-tests/-'
                         '/tree/main/apps/level1/cng_test/src/README.md',
        'Source': 'https://gitlab.com/ska-telescope/sdp/ska-sdp-benchmark-tests/-'
                  '/tree/main/apps/level1/cng_test/src',
    },
    zip_safe=False,
    classifiers=[
        'Development Status :: Alpha',
        'Intended Audience :: Developers',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.7',
        'Programming Language :: Python :: 3.8',
    ],
    packages=packages,
    package_data=package_data,
    scripts=['bin/cngtest'],
    # install_requires=reqs,
)
