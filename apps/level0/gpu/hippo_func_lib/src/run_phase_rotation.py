# See the LICENSE file at the top-level directory of this distribution.

"""
.py file to run phase rotate function from ska-sdp-func.
Runs CPU version of function and GPU version if GPU is present.
Accepts command line arguments for input variables, as shown below.
Outputs elapsed time for CPU and GPU (if available) separately

ska-sdp-func must be installed as detailed here:
https://developer.skao.int/projects/ska-sdp-func/en/latest/install.html

usage: reframe_test_phase_rotate.py [-h] [-s START] [-n STEP] [-p POLS] [-c CHANNELS] [-b BASELINES] [-t TIMES] [-d {0,1,2}]

Phase rotate function benchmark test

optional arguments:
  -h, --help                            show this help message and exit
  -s START, --start START               Channel start in Hz
  -n STEP, --step STEP                  Channel step in Hz
  -p POLS, --pols POLS                  Number of polarisations
  -c CHANNELS, --channels CHANNELS      Number of channels
  -b BASELINES, --baselines BASELINES   Number of baselines
  -t TIMES, --times TIMES               Number of times
  -d {0,1,2}, --device {0,1,2}          Device to run the DFT on, 0 = CPU, 1 = GPU, 2 = CPU and GPU

Output format for sanity check =

Phase Rotation on XPU:
Channel Start                   XXXXXXXXXXX Hz
Channel Step                    XXXXXXXXXXX Hz
Channels                        XXXX
Polarisations                   X
Baselines                       XXXX
Times                           XXXX
Elapsed Time                    XXXX s
"""

import time
import argparse
import numpy

try:
    import cupy
except ImportError:
    cupy = None

from ska_sdp_func.visibility import phase_rotate_uvw, phase_rotate_vis
from ska_sdp_func.utility import SkyCoord

# Create the parser
my_parser = argparse.ArgumentParser(description='Phase rotate function benchmark test')

my_parser.add_argument('-s',
                       '--start',
                       action='store',
                       help='Channel start in Hz',
                       type=int,
                       default=100e6)

my_parser.add_argument('-n',
                       '--step',
                       action='store',
                       help='Channel step in Hz',
                       type=int,
                       default=10e6)

my_parser.add_argument('-p',
                       '--pols',
                       action='store',
                       help='Number of polarisations',
                       type=int,
                       default=4)

my_parser.add_argument('-c',
                       '--channels',
                       action='store',
                       help='Number of channels',
                       type=int,
                       default=10)

my_parser.add_argument('-b',
                       '--baselines',
                       action='store',
                       help='Number of baselines',
                       type=int,
                       default=351)

my_parser.add_argument('-t',
                       '--times',
                       action='store',
                       help='Number of times',
                       type=int,
                       default=10)

my_parser.add_argument('-d',
                       '--device',
                       action='store',
                       choices=[0, 1, 2],
                       help='Device to run Phase Rotate on, 0 = CPU, 1 = GPU, 2 = CPU and GPU',
                       type=int,
                       default=2)

# Execute parse_args()
args = my_parser.parse_args()

channel_start_hz = args.start
channel_step_hz = args.step
num_pols = args.pols
num_channels = args.channels
num_baselines = args.baselines
num_times = args.times
device = args.device

def print_details(device_name, elapsed_time, channel_start, channel_step, 
                    pols, channels, baselines, times):

    print(f'\nPhase Rotation on {device_name}:')
    print(f'Channel Start \t\t\t{channel_start} Hz')
    print(f'Channel Step \t\t\t{channel_step} Hz')
    print(f'Channels \t\t\t{channels}')
    print(f'Polarisations \t\t\t{pols}')
    print(f'Baselines \t\t\t{baselines}')
    print(f'Times \t\t\t\t{times}')
    print(f'Elapsed Time \t\t\t{elapsed_time:.5f} s')


"""Test phase centre rotation."""
original_phase_centre = SkyCoord(
    "icrs", 123.5 * numpy.pi / 180.0, 17.8 * numpy.pi / 180.0
)
new_phase_centre = SkyCoord(
    "icrs", 148.3 * numpy.pi / 180.0, 38.9 * numpy.pi / 180.0
)


# Create input and output arrays
uvw_in = numpy.random.random_sample([num_times, num_baselines, 3])
uvw_out = numpy.zeros_like(uvw_in)
vis_in = (
    numpy.random.random_sample(
        [num_times, num_baselines, num_channels, num_pols]
    )
    + 0j
)
vis_out = numpy.zeros_like(vis_in)

if device in (0, 2):
# Run test on CPU, using numpy arrays.
    # CPU start time
    cpu_start_time = time.time()
    # Run on CPU
    print("Testing phase rotation on CPU from ska-sdp-func...")
    phase_rotate_uvw(
        original_phase_centre,
        new_phase_centre,
        uvw_in,
        uvw_out,
    )
    phase_rotate_vis(
        original_phase_centre,
        new_phase_centre,
        channel_start_hz,
        channel_step_hz,
        uvw_in,
        vis_in,
        vis_out,
    )
    # CPU end time
    cpu_end_time = time.time() - cpu_start_time

    print_details('CPU', cpu_end_time, channel_start_hz, channel_step_hz, 
                num_pols, num_channels, num_baselines, num_times)

if device in (1, 2):
    # Run test on GPU, using cupy arrays.
    if cupy:
        uvw_in_gpu = cupy.asarray(uvw_in)
        uvw_out_gpu = cupy.zeros_like(uvw_in_gpu)
        vis_in_gpu = cupy.asarray(vis_in)
        vis_out_gpu = cupy.zeros_like(vis_in_gpu)

        # GPU start time
        gpu_start_time = time.time()
        print("Testing phase rotation on GPU from ska-sdp-func...")
        phase_rotate_uvw(
            original_phase_centre,
            new_phase_centre,
            uvw_in_gpu,
            uvw_out_gpu,
        )
        phase_rotate_vis(
            original_phase_centre,
            new_phase_centre,
            channel_start_hz,
            channel_step_hz,
            uvw_in_gpu,
            vis_in_gpu,
            vis_out_gpu,
        )
        gpu_end_time = time.time() - gpu_start_time

        print_details('GPU', gpu_end_time, channel_start_hz, channel_step_hz, 
                    num_pols, num_channels, num_baselines, num_times)

    else:
            print('\nGPU unavailable')
