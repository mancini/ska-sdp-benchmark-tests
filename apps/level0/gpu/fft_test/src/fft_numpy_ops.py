"""Sample numpy operations"""

from time import time
import numpy as np
import cupy as cp


print('Numpy version:', np.__version__)
print('Cupy version:', cp.__version__)

np.__config__.show()

# Let's take the randomness out of random numbers (for reproducibility)
np.random.seed(0)
cp.random.seed(0)

sizes = [
    2**5,   # 32
    2**7,   # 128
    2**9,   # 512
    2**11,  # 2048
    2**13   # 8192
]

N = 200

for size in sizes:
    # Real FFT on GPU
    A = cp.random.random((size, size))
    t = time()
    for i in range(N):
        cp.fft.fft2(A)

    delta = time() - t
    print(f"2D FFT of a real {size:d}x{size:d} matrix in {delta / N:0.6f} s.")

    # Real IFFT on GPU
    A = cp.fft.fft2(A)
    t = time()
    for i in range(N):
        cp.fft.ifft2(A)

    delta = time() - t
    print(f"2D IFFT of a real {size:d}x{size:d} matrix in {delta / N:0.6f} s.")
    del A

    # Complex FFT on GPU
    A = cp.random.random((size, size)) + cp.random.random((size, size)) * 1j
    t = time()
    for i in range(N):
        cp.fft.fft2(A)

    delta = time() - t
    print(f"2D FFT of a complex {size:d}x{size:d} matrix in {delta / N:0.6f} s.")

    # Complex IFFT on GPU
    A = cp.fft.fft2(A)
    t = time()
    for i in range(N):
        cp.fft.ifft2(A)

    delta = time() - t
    print(f"2D IFFT of a complex {size:d}x{size:d} matrix in {delta / N:0.6f} s.")
    del A
