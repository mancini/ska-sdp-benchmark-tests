# FFT Benchmarks with GPU acceleration
This app contains the benchmarks for GPU accelerated FFT calculations using NumPy.

The Jupyter notebook `FFT_GPU.ipynb` can be used to plot all the benchmark metrics that are extracted during benchmarking tests. The notebook must be invoked from the current directory. If not, please change the repository root and ReFrame root directories in the notebook preamble.
