# pylint: disable=C0301
"""
NCCL performance benchmarks
------------------------------

Context
~~~~~~~~~~~~~~~~~~~~~~~~

`NCCL <https://docs.nvidia.com/deeplearning/nccl/user-guide/docs/index.html>`_ is a stand-alone
library of standard communication routines for GPUs, implementing all-reduce, all-gather,
reduce, broadcast, reduce-scatter, as well as any send/receive based communication pattern. It
has been optimized to achieve high bandwidth on platforms using PCIe, NVLink, NVswitch, as well as
networking using InfiniBand Verbs or TCP/IP sockets.

In this test, we are only interested in the intra-node communication latencies and bandwidths and
so, we run this test on single node with multiple GPUs. The benchmarks report the so-called bus
bandwidth that can be used to compare with underlying hardware peak bandwidth for collective
communications. More details on how the bus bandwidth is estimated can be found at `nccl tests
repository <https://github.com/NVIDIA/nccl-tests/blob/master/doc/PERFORMANCE.md>`_.

.. note::
  Each benchmark runs in two different modes namely, in-place and out-of-place. An in-place
  operation uses the same buffer for its output as was used to provide its input. An out-of-place
  operation has distinct input and output buffers.

Test variants
~~~~~~~~~~~~~~~~~~~~~~~~

The test is parameterised to run following communication benchmarks:

- ``sendrecv``
- ``gather``
- ``scatter``
- ``reduce``
- ``all_gather``
- ``all_reduce``

A specific test can be chosen at the runtime using ``-t`` flag on CLI. An example is shown in the
:ref:`nccl usage`.

Test configuration
~~~~~~~~~~~~~~~~~~~~~~~~

The tests can be configured to change the minimum and maximum sizes of the messages that will be
used in benchmarks. They can be configured at the runtime using ``min_size`` and ``max_size``
variables. The default values are 8 bytes and 128 MiB, respectively.


.. _nccl usage:

Usage
~~~~~~~~~~~~~~~~~~~~~~~~

The test can be run using following commands.

.. code-block:: bash

  cd ska-sdp-benchmark-tests
  conda activate ska-sdp-benchmark-tests
  reframe/bin/reframe -C reframe_config.py -c apps/level0/gpu/nccl_test/reframe_nccltest.py --run --performance-report

To run only ``scatter`` and ``gather`` variants and skip rest of the benchmarks, use ``-t`` flag as
follows:

.. code-block:: bash

  cd ska-sdp-benchmark-tests
  conda activate ska-sdp-benchmark-tests
  reframe/bin/reframe -C reframe_config.py -c apps/level0/gpu/nccl_test/reframe_nccltest.py -t scatter$ -t gather$ --run --performance-report

To change the default value of ``min_size`` during runtime, use ``-S`` flag. For example to use
1 MiB of ``min_size``:

.. code-block:: bash

  cd ska-sdp-benchmark-tests
  conda activate ska-sdp-benchmark-tests
  reframe/bin/reframe -C reframe_config.py -c apps/level0/gpu/nccl_test/reframe_nccltest.py -S min_size=1M --run --performance-report

Test class documentation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
"""
# pylint: enable=C0301

import math

import reframe as rfm
import reframe.utility.sanity as sn  # pylint: disable=import-error
from modules.base_class import RunOnlyBenchmarkBase, CompileOnlyBenchmarkBase
from modules.reframe_extras import MultiRunMixin, UsesInternet
from reframe.core.backends import getlauncher  # pylint: disable=import-error
import reframe.utility.osext as osext  # pylint: disable=import-error

from modules.utils import filter_systems_by_env
from reframe.core.builtins import run_before, fixture, parameter, variable, run_after, performance_function, \
    sanity_function


# pylint: disable=C0116,E0401,E0602,E1101,W0201


class NcclTestDownload(RunOnlyBenchmarkBase, UsesInternet):
    """Fixture to fetch NCCL test source code"""
    
    descr = 'Fetch source code of NCCL test'

    def __init__(self):
        super().__init__()
        self.local = True
        self.sourcesdir = 'https://github.com/NVIDIA/nccl-tests.git'
        self.executable = 'echo noop'

    @run_before('sanity')
    def set_sanity_patterns(self):
        """Set sanity patterns"""
        self.sanity_patterns = sn.assert_not_found('error', self.stderr)
        
        
class NcclTestBuild(CompileOnlyBenchmarkBase, MultiRunMixin):
    """NCCL tests compile test"""
    
    descr = 'Build NCCL tests from sources'
    
    # Share resource from fixture
    nccl_test_src = fixture(NcclTestDownload, scope='session')

    def __init__(self):
        # Cross compilation is not possible on certain g5k clusters. We force
        # the job to be non-local so building will be on remote node
        super().__init__()
        if 'g5k' in self.current_system.name:
            self.build_locally = False

    @run_before('compile')
    def set_sourcedir(self):
        """Set source directory from dependencies"""
        self.sourcesdir = self.nccl_test_src.stagedir

    @run_before('compile')
    def set_build_system_opts(self):
        """Set build system options"""
        self.prebuild_cmds = [
            'echo CUDA_HOME=$CUDA_HOME',
            'echo CUDA_ROOT=$CUDA_ROOT',
            'echo NCCL_HOME=$NCCL_HOME',
            'echo NCCL_ROOT=$NCCL_ROOT',
            'echo MPI_HOME=$MPI_HOME',
            'echo MPI_ROOT=$MPI_ROOT',
        ]
        self.build_system = 'Make'
        self.build_system.options = [
           'CUDA_HOME=$CUDA_ROOT',
           'NCCL_HOME=$NCCL_ROOT',
           'MPI_HOME=$MPI_ROOT',
            'MPI=1',
        ]
        self.build_system.max_concurrency = 8

    @sanity_function
    def validate_build(self):
        # If compilation fails, the test would fail in any case, so nothing to
        # further validate here.
        return True


@rfm.simple_test
class NcclPerfTest(RunOnlyBenchmarkBase, MultiRunMixin):
    """NCCL performance tests main class"""

    descr = 'NCCL perf tests to measure collective GPU communications latency and bandwidth'

    # Variants of the test
    comm_type = parameter(['sendrecv', 'gather', 'scatter', 'reduce', 'all_gather', 'all_reduce'])

    # Variable to define number of nodes
    num_nodes = variable(int, value=1)

    # Minimum size of message
    min_msg = variable(str, value='8')

    # Maximum size of message
    max_msg = variable(str, value='128M')
    
    # NCCL test binaries
    nccl_test_bin = fixture(NcclTestBuild, scope='environment')

    def __init__(self):
        super().__init__()
        self.valid_prog_environs = [
            'nccl-test',
        ]
        self.valid_systems = filter_systems_by_env(self.valid_prog_environs)
        self.maintainers = [
            'Mahendra Paipuri (mahendra.paipuri@inria.fr)'
        ]
        self.exclusive_access = True
        self.time_limit = '0d0h10m0s'
        self.tags |= {
            self.comm_type,
        }
        self.perf_ind = {
            'sendrecv': 3,
            'gather': 4,
            'scatter': 4,
            'reduce': 5,
            'all_gather': 3,
            'all_reduce': 4,
        }

    @run_after('init')
    def gen_msg_sizes(self):
        """Generate list of message sizes"""
        def convert_to_bytes(size_string):
            convert_factors = {
                'K': 1024,
                'M': 1024 * 1024,
                'G': 1024 * 1024 * 1024,
            }
            try:
                size_int = int(size_string)
            except ValueError:
                for units, factor in convert_factors.items():
                    if units in size_string:
                        size_int = int(size_string.split(units)[0]) * factor
            return size_int

        min_msg_bytes = convert_to_bytes(self.min_msg)
        max_msg_bytes = convert_to_bytes(self.max_msg)

        n_msg_sizes = int(math.log(max_msg_bytes / min_msg_bytes) / math.log(2) + 1)
        self.msg_sizes = [min_msg_bytes * 2 ** i for i in range(n_msg_sizes)]

    @run_after('setup')
    def set_sourcesdir(self):
        """Set source directory"""
        self.sourcesdir = self.nccl_test_bin.stagedir
        
    @run_after('setup')
    def set_git_commit_tag(self):
        """Fetch git commit hash"""
        git_ref = osext.git_repo_hash(short=False, wd=self.prefix)
        self.tags |= {f'git={git_ref}'}

    @run_after('setup')
    def set_num_tasks_reservation(self):
        """Set number of tasks for job reservation"""
        # This method sets number of tasks for the reservation. Here ONLY resources are reserved
        if self.current_partition.scheduler.registered_name == 'oar':
            self.num_tasks_per_node = \
                (self.current_partition.processor.num_cpus
                 // self.current_partition.processor.num_cpus_per_core)
        else:
            self.num_tasks_per_node = self.current_partition.processor.num_cpus
        self.num_tasks = self.num_nodes * self.num_tasks_per_node

    @run_after('setup')
    def set_tags(self):
        """Add tags to the test"""
        self.tags |= {
            f'num_nodes={self.num_nodes}',
            f'comm_type={self.comm_type}',
            f'min_msg_size={self.min_msg}',
            f'max_msg_size={self.max_msg}',
            f'num_gpus={self.current_partition.devices[0].num_devices}',
        }

    @run_after('setup')
    def set_launcher(self):
        """Set launcher to local to avoid appending mpirun or srun"""
        self.job.launcher = getlauncher('local')()

    @run_before('run')
    def set_executable(self):
        """Set executable name"""
        self.executable = f'./build/{self.comm_type}_perf'
        self.executable_opts = [
            f'-b {self.min_msg}',
            f'-e {self.max_msg}',
            f'-f 2 -g {self.current_partition.devices[0].num_devices}',
        ]

    @run_before('sanity')
    def set_sanity_patterns(self):
        """Set sanity patterns. Example stdout:

        .. code-block:: text

            # # Out of bounds values : 0 OK
            # # Avg bus bandwidth    : 0.791943

        """
        self.sanity_patterns = sn.all([
            sn.assert_found('# Out of bounds values', self.stdout),
            sn.assert_found('# Avg bus bandwidth', self.stdout),
        ])

    def parse_stdout(self, msg_size, place, ind):
        """Read stdout file to extract perf variables"""
        ind = {
            'in': ind,
            'out': ind + 4,
        }
        with open(self.stdout.evaluate()) as f:
            for line in f:
                if '#        (B)    (elements)' in line:
                    while '# Out of bounds values' not in line:
                        line = next(f)
                        row = line.split()
                        if str(msg_size) in row:
                            return float(row[ind[place]])
        return 0

    @performance_function('GB/s')
    def extract_algbw(self, msg_size=None, place='in'):
        """Performance function to extract algorithmic bandwidth"""
        if msg_size is None:
            msg_size = self.min_msg
        return self.parse_stdout(msg_size, place, ind=self.perf_ind[self.comm_type] + 1)

    @performance_function('GB/s')
    def extract_busbw(self, msg_size=None, place='in'):
        """Performance function to extract bus bandwidth"""
        if msg_size is None:
            msg_size = self.min_msg
        return self.parse_stdout(msg_size, place, ind=self.perf_ind[self.comm_type] + 2)

    @performance_function('us')
    def extract_time(self, msg_size=None, place='in'):
        """Performance function to extract latency"""
        if msg_size is None:
            msg_size = self.min_msg
        return self.parse_stdout(msg_size, place, ind=self.perf_ind[self.comm_type])

    @run_before('performance')
    def set_perf_patterns(self):
        # pylint: disable=C0301
        """Set performance variables. Sample stdout:

        .. code-block:: text

            # # nThread 1 nGpus 2 minBytes 8 maxBytes 134217728 step: 2(factor) warmup iters: 5 iters: 20 validation: 1
            # #
            # # Using devices
            # #   Rank  0 Pid  16042 on grouille-1 device  0 [0x21] A100-PCIE-40GB
            # #   Rank  1 Pid  16042 on grouille-1 device  1 [0x81] A100-PCIE-40GB
            # #
            # #                                               out-of-place                       in-place
            # #       size         count      type     time   algbw   busbw  error     time   algbw   busbw  error
            # #        (B)    (elements)               (us)  (GB/s)  (GB/s)            (us)  (GB/s)  (GB/s)
            #            8             2     float    23.17    0.00    0.00  0e+00    22.75    0.00    0.00  0e+00
            #           16             4     float    22.65    0.00    0.00  0e+00    22.68    0.00    0.00  0e+00
            #           32             8     float    22.44    0.00    0.00  0e+00    22.54    0.00    0.00  0e+00
            #           64            16     float    22.83    0.00    0.00  0e+00    22.37    0.00    0.00  0e+00
            #          128            32     float    22.72    0.01    0.01  0e+00    22.64    0.01    0.01  0e+00
            #          256            64     float    22.67    0.01    0.01  0e+00    22.47    0.01    0.01  0e+00
            #          512           128     float    22.42    0.02    0.02  0e+00    22.26    0.02    0.02  0e+00
            #         1024           256     float    22.63    0.05    0.05  0e+00    22.50    0.05    0.05  0e+00
            #         2048           512     float    22.47    0.09    0.09  0e+00    22.52    0.09    0.09  0e+00
            #         4096          1024     float    23.33    0.18    0.18  0e+00    23.28    0.18    0.18  0e+00
            #         8192          2048     float    25.22    0.32    0.32  0e+00    24.90    0.33    0.33  0e+00
            #        16384          4096     float    33.57    0.49    0.49  0e+00    33.95    0.48    0.48  0e+00
            #        32768          8192     float    48.07    0.68    0.68  0e+00    49.19    0.67    0.67  0e+00
            #        65536         16384     float    68.66    0.95    0.95  0e+00    72.52    0.90    0.90  0e+00
            #       131072         32768     float    115.3    1.14    1.14  0e+00    114.1    1.15    1.15  0e+00
            #       262144         65536     float    176.9    1.48    1.48  0e+00    174.8    1.50    1.50  0e+00
            #       524288        131072     float    334.0    1.57    1.57  0e+00    342.7    1.53    1.53  0e+00
            #      1048576        262144     float    643.3    1.63    1.63  0e+00    599.7    1.75    1.75  0e+00
            #      2097152        524288     float   1125.6    1.86    1.86  0e+00   1077.9    1.95    1.95  0e+00
            #      4194304       1048576     float   2813.4    1.49    1.49  0e+00   2670.2    1.57    1.57  0e+00
            #      8388608       2097152     float   5561.3    1.51    1.51  0e+00   5497.1    1.53    1.53  0e+00
            #     16777216       4194304     float    11070    1.52    1.52  0e+00    10950    1.53    1.53  1e+00
            #     33554432       8388608     float    22215    1.51    1.51  0e+00    22687    1.48    1.48  1e+00
            #     67108864      16777216     float    45987    1.46    1.46  0e+00    46600    1.44    1.44  1e+00
            #    134217728      33554432     float    95433    1.41    1.41  0e+00    96707    1.39    1.39  1e+00
            # # Out of bounds values : 0 OK
            # # Avg bus bandwidth    : 0.778536
            # #

        """
        # pylint: enable=C0301
        self.perf_variables = {
            f'algbw_in_{self.min_msg}': self.extract_algbw(),
            f'busbw_in_{self.min_msg}': self.extract_busbw(),
            f'time_in_{self.min_msg}': self.extract_time(),
            f'algbw_out_{self.min_msg}': self.extract_algbw(place='out'),
            f'busbw_out_{self.min_msg}': self.extract_busbw(place='out'),
            f'time_out_{self.min_msg}': self.extract_time(place='out'),
        }
        for perf in ['algbw', 'busbw', 'time']:
            for place in ['in', 'out']:
                for msg_size in self.msg_sizes[1:]:
                    self.perf_variables[f'{perf}_{place}_{msg_size}'] = \
                        getattr(self, f'extract_{perf}')(msg_size, place)

    @run_before('performance')
    def set_reference_values(self):
        """Set reference perf values"""
        self.reference = {
            '*': {
                '*': (None, None, None, '*'),
            }
        }
