# NCCL benchmarks

This app contains NCCL benchmark tests to estimate latency and bandwidth for multi GPU nodes. More details on benchmark,
instructions to run are detailed in the
[documentation](https://developer.skao.int/projects/ska-sdp-benchmark-tests/en/latest/content/benchmarks.html#nccl-performance-benchmarks).

The Jupyter notebook `NCCL.ipynb` can be used to tabulate and plot memory bandwidths for
different communication primitives that are used in the benchmarking tests. The notebook must be invoked
from the `ska-sdp-benchmark-tests` root directory.
