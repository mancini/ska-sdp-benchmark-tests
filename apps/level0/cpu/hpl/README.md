# HPL benchmark

This app contains HPL benchmark test. More details on benchmark, instructions to run are detailed in the [documentation](https://developer.skao.int/projects/ska-sdp-benchmark-tests/en/latest/content/benchmarks.html#hpl-benchmark). The configuration files `HPL.dat` for different systems and partitions are located in `input/` folder. 

The Jupyter notebook `HPL.ipynb` can be used to tabulate and plot `Gflop/s` metric that are extracted during benchmarking tests. The notebook must be invoked from the `ska-sdp-benchmark-tests` root directory.
