# pylint: disable=C0301
"""
HPCG benchmark
----------------------------

Context
~~~~~~~~~~~~~~~~~~~~~~~~

This is HPCG microbenchmark test with a default problem size of 104. The benchmark does several
matrix vector operations on sparse matrices. More details about the benchmark can be found at
`HPCG benchmark website <https://www.hpcg-benchmark.org/>`_.

.. note::
  Currently, the implemented test uses the optimized version of benchmark shipped by Intel MKL
  library. We use GNU compiler toolchain for running the test on AMD processors. For the case
  of IBM POWER processors, IBM shipped XL compiler toolchain is used to run the benchmark

Test size
~~~~~~~~~~~~~~~~~~~~~~~~

Currently, two different variables can be controlled for running tests. They are

- number of nodes to run the benchmark
- problem size

By default, benchmark will run on single node. If the user wants to run on multiple nodes, we can
set the ``num_nodes`` variable at the run time. Similarly, the default problem size is 104 and it
can be set at runtime using ``problem_size`` variable.

.. note::
  Even if more than one node is used in the test, the resulting performance metric, ``Gflop/s``,
  is always reported per node.

.. note::
  The problem size must be a multiple of 8. This is the requirement from the HPCG benchmark
  *per se*.


Test types
~~~~~~~~~~~~~~~~~~~~~~~~

Currently there are three different types of tests implemented:

- ``HpcgXlTest``: HPCG with IBM XL toolchain
- ``HpcgGnuTest```: HPCG with GNU GCC toolchain
- ``HpcgMklTest``: HPCG shipped with Intel MKL package

If a system has two valid tests, we can restrict the test using ``-n`` flag on the CLI. It is 
shown in :ref:`hpcg usage`.

.. _hpcg usage:

Usage
~~~~~~~~~~~~~~~~~~~~~~~~

The test can be run using following commands.

.. code-block:: bash

  cd ska-sdp-benchmark-tests
  conda activate ska-sdp-benchmark-tests
  reframe/bin/reframe -C reframe_config.py -c apps/level0/cpu/hpcg/reframe_hpcg.py --run --performance-report


We can set number of nodes on the CLI using:

.. code-block:: bash

  cd ska-sdp-benchmark-tests
  conda activate ska-sdp-benchmark-tests
  reframe/bin/reframe -C reframe_config.py -c apps/level0/cpu/hpcg/reframe_hpcg.py --run --performance-report -S num_nodes=2

Similarly, problem size of the benchmark can be altered at runtime using:

.. code-block:: bash

  cd ska-sdp-benchmark-tests
  conda activate ska-sdp-benchmark-tests
  reframe/bin/reframe -C reframe_config.py -c apps/level0/cpu/hpcg/reframe_hpcg.py --run --performance-report -S problem_size=120

For instance, if a system has both ``HpcgGnuTest`` and ``HpcgMklTest`` as valid tests and if we want to run 
only ``HpcgMklTest``, we can use ``-n`` flag as follows:

.. code-block:: bash

  cd ska-sdp-benchmark-tests
  conda activate ska-sdp-benchmark-tests
  reframe/bin/reframe -C reframe_config.py -c apps/level0/cpu/hpcg/reframe_hpcg.py --run --performance-report -n HpcgMklTest

We can run multiple tests corresponding to different MPI configurations
.. code-block:: bash

  cd ska-sdp-benchmark-tests
  conda activate ska-sdp-benchmark-tests
  vary_MPI=1 reframe/bin/reframe -C reframe_config.py -c apps/level0/cpu/hpcg/reframe_hpcg.py --run --performance-report

Similarly, we can vary the problem size of the benchmark using:

.. code-block:: bash

  cd ska-sdp-benchmark-tests
  conda activate ska-sdp-benchmark-tests
  vary_problem_size=1 reframe/bin/reframe -C reframe_config.py -c apps/level0/cpu/hpcg/reframe_hpcg.py --run --performance-report



Test class documentation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
"""
# pylint: enable=C0301

import os

import reframe as rfm
import reframe.utility.sanity as sn  # pylint: disable=import-error
import reframe.utility.osext as osext
from modules.base_class import BenchmarkBase, RunOnlyBenchmarkBase

from modules.utils import filter_systems_by_env
from modules.reframe_extras import patch_launcher_command, MultiRunMixin, FetchSourcesBase
from reframe.core.builtins import variable, fixture, run_after, run_before, performance_function, parameter


# pylint: disable=C0116,E0401,E0602,E1101,W0201,W0231


class HpcgMixin(rfm.RegressionMixin):
    """Common regression test attributes for HpcgGnuTest and HpcgMklTest"""
    vary_MPI = variable(int, value=int(os.getenv('vary_MPI', default=0)))
    vary_problem_size = variable(int, value=int(os.getenv('vary_problem_size', default=0)))

    # Variables to define number of nodes
    if vary_MPI == 1:
        # number of nodes is set to 1, 2 and 4, based on the range() function with values (0, 3).
        num_nodes = parameter(1 << i for i in range(0, 3))
    else:
        # Default value number of node = 1
        num_nodes = variable(int, value=1)

    # Problem size (must be multiple of 8)
    if vary_problem_size == 1:
        # Problem size is set to 8, 16 and 32, based on the range() function with values (0, 3).
        problem_size = parameter((1 << i) * 8 for i in range(0, 3))
    else:
        # Default value problem size =16
        problem_size = variable(int, value=16)

    @run_after('init')
    def test_settings(self):
        """Common test settings"""
        self.exclusive_access = True
        self.time_limit = '0d2h0m0s'
        self.maintainers = ['Mahendra Paipuri (mahendra.paipuri@inria.fr)']

    @run_after('setup')
    def set_git_commit_tag(self):
        """Fetch git commit hash"""
        git_ref = osext.git_repo_hash(short=False, wd=self.prefix)
        self.tags |= {f'git={git_ref}'}

    @run_after('setup')
    def set_executable_opts(self):
        """Set executable options"""
        self.executable_opts = [
            f'--nx={self.problem_size}', f'--ny={self.problem_size}',
            f'--nz={self.problem_size}', '--rt=1800',
        ]

    @run_before('run')
    def export_env_vars(self):
        """Export env variables using OMPI_MCA param for OpenMPI"""
        # This is needed for g5k clusters as env vars are not exported to all nodes in
        # the reservation
        self.env_vars['OMPI_MCA_mca_base_env_list'] = \
            f'\"PATH;LD_LIBRARY_PATH;{";".join(self.env_vars.keys())}\"'

    @run_before('run')
    def set_tags(self):
        """Add tags to the test"""
        self.tags |= {
            f'num_nodes={self.num_nodes}',
            f'num_procs={self.num_tasks}',
            f'problem_size={self.problem_size}',
        }
    
    @run_before('run')
    def set_keep_files(self):
        """List of files to keep in output"""
        self.keep_files = [self.output_file_patt]

    @run_before('sanity')
    def set_sanity_patterns(self):
        """
        Set sanity patterns. Example stdout:

        .. code-block: text

           # Final Summary::HPCG result is VALID with a GFLOP/s rating of=2.70225

        """
        self.sanity_patterns = sn.assert_found('HPCG result is VALID', self.output_file)

    @performance_function('Gflop/s')
    def extract_gflops(self):
        """Performance extraction function for Gflops"""
        return (sn.extractsingle(
            r'HPCG result is VALID with a GFLOP\/s rating of=\s*'
            r'(?P<perf>\S+)',
            self.output_file, 'perf', float) / self.num_nodes)

    @run_before('performance')
    def set_perf_patterns(self):
        """Set performance variables"""
        self.perf_variables = {
            'Gflop/s': self.extract_gflops(),
        }

    @run_before('performance')
    def set_reference_values(self):
        self.reference = {
            '*': {
                'Gflop/s': (None, None, None, 'Gflop/s'),
            }
        }


class HpcgXlDownloadTest(FetchSourcesBase):
    descr = 'Fetch source code of HPCG GNU Benchmark'
    sourcesdir = 'https://github.com/IBM/HPCG.git'
    cnd_env_name = "rfm_HpcgXlTest"


@rfm.simple_test
class HpcgXlTest(BenchmarkBase, HpcgMixin, MultiRunMixin):
    """Main class of HPCG test based on IBM Xl"""

    descr = 'HPCG reference benchmark based on IBM POWER9 processors using Xl toolchain'
    hpcg_xl_source = fixture(HpcgXlDownloadTest, scope="partition")

    def __init__(self):
        super().__init__()
        self.valid_prog_environs = ['xlc-hpcg']
        self.valid_systems = filter_systems_by_env(self.valid_prog_environs)

    @run_before('compile')
    def build_executable(self):
        """Set build system and config options"""
        self.sourcesdir = self.hpcg_xl_source.stagedir

        self.build_system = 'Make'
        self.build_system.max_concurrency = 8
        builddir = os.path.join(self.stagedir, 'build')
        self.prebuild_cmds = [
            f'mkdir -p {builddir}',
            f'cd {builddir}',
            f"../configure {self.current_partition.extras['hpcg_arch']}",
        ]

    @run_after('setup')
    def set_num_tasks_threads(self):
        """Set number of MPI and OpenMP tasks"""
        self.num_tasks_per_node = 2 * (self.current_partition.processor.num_cpus 
                                       // self.current_partition.processor.num_cpus_per_core)
        self.num_tasks = self.num_nodes * self.num_mpi_tasks_per_node
        self.num_cpus_per_task = 2
        self.variables = {
            'OMP_NUM_THREADS': str(self.num_cpus_per_task),
            'OMP_PROC_BIND': 'FALSE',
            'OMP_WAIT_POLICY': 'ACTIVE',
        }

    @run_after('setup')
    def set_executable(self):
        """Set executable"""
        self.executable = 'build/bin/xhpcg'

    @run_after('setup')
    def set_outfile(self):
        """Set name of output file"""
        self.output_file_patt = 'HPCG*.txt'
        self.output_file = sn.getitem(sn.glob(self.output_file_patt), 0)

    @run_before('run')
    def job_launcher_opts(self):
        """Set job launcher options"""
        self.job.launcher.options = [
            '--bind–to core', '--map-by core'
        ]


class HpcgGnuDownloadTest(FetchSourcesBase):
    descr = 'Fetch source code of HPCG GNU Benchmark'
    sourcesdir = 'https://github.com/hpcg-benchmark/hpcg.git'
    cnd_env_name = "rfm_HpcgGnuTest"


@rfm.simple_test
class HpcgGnuTest(BenchmarkBase, HpcgMixin, MultiRunMixin):
    """Main class of HPCG test based on GNU"""

    descr = 'HPCG reference benchmark based on AMD processors using GNU toolchain'
    hpcg_sources = fixture(HpcgGnuDownloadTest, scope="partition")
    
    # Variable to define compiler architecture
    arch = variable(str, value='AMD_MPI')

    def __init__(self):
        super().__init__()
        self.valid_prog_environs = ['gnu-hpcg']
        self.valid_systems = filter_systems_by_env(self.valid_prog_environs)
        # Cross compilation might not be always possible on Grid50000 clusters
        if 'g5k' in self.current_system.name:
            self.build_locally = False
        
    @run_after('setup')
    def set_num_tasks(self):
        """Set number of tasks for job"""
        if self.current_partition.scheduler.registered_name == 'oar':
            self.num_tasks_per_node = \
                (self.current_partition.processor.num_cpus
                 // self.current_partition.processor.num_cpus_per_core)
        else:
            # Setting the number of tasks to 1 on each compute node.
            self.num_tasks_per_node = 1
        self.num_tasks = self.num_nodes * self.num_tasks_per_node
        self.num_cpus_per_task = self.current_partition.processor.num_cpus

    @run_before('compile')
    def set_prebuild_cmds(self):
        """Copy Make file into setup folder"""
        self.sourcesdir = self.hpcg_sources.stagedir
        makefile_path = os.path.join(self.prefix, 'makes', f'Make.{self.arch}')
        self.prebuild_cmds = [
            f'cp {makefile_path} {self.stagedir}/setup'
        ]
    
    @run_before('compile')
    def build_executable(self):
        """Set build system and options"""
        self.build_system = 'Make'
        self.build_system.options = [f'arch={self.arch}']

    @run_after('setup')
    def set_executable(self):
        """Set executable"""
        self.executable = 'bin/xhpcg'

    @run_after('setup')
    def set_outfile(self):
        """Set name of output file"""
        self.output_file_patt = 'HPCG*.txt'
        self.output_file = sn.getitem(sn.glob(self.output_file_patt), 0)

    @run_before('run')
    def job_launcher_opts(self):
        """Set job launcher options"""
        if self.job.launcher.registered_name == 'mpirun':
            self.job.launcher.options = [
                f'-npernode {self.num_tasks_per_node}'
            ]


@rfm.simple_test
class HpcgMklTest(RunOnlyBenchmarkBase, HpcgMixin, MultiRunMixin):
    """Main class of HPCG test based on MKL"""

    descr = 'HPCG reference benchmark based on Intel processors using MKL toolchain'

    def __init__(self):
        super().__init__()
        self.valid_prog_environs = ['intel-hpcg']
        self.valid_systems = filter_systems_by_env(self.valid_prog_environs)
        self.sourcesdir = None
        
    @run_after('setup')
    def set_num_tasks(self):
        """Set number of tasks for job"""
        if self.current_partition.scheduler.registered_name == 'oar':
            self.num_tasks_per_node = \
                (self.current_partition.processor.num_cpus
                 // self.current_partition.processor.num_cpus_per_core)
            self.num_cpus_per_task = self.current_partition.processor.num_cpus
            self.job.launcher.command = patch_launcher_command
            self.job.launcher.options = [f'-n {self.num_nodes}', '-ppn 1']
        else:
            self.num_tasks_per_node = 1
            self.num_cpus_per_task = self.current_partition.processor.num_cpus
        self.num_tasks = self.num_nodes * self.num_tasks_per_node

    @run_after('setup')
    def set_env_vars(self):
        """Set job specific env variables"""
        self.variables = {
            'OMP_NUM_THREADS': str(self.current_partition.processor.num_cpus),
        }

    @run_after('setup')
    def set_executable(self):
        """Set executable"""
        self.executable = '$XHPCG_BIN'

    @run_after('setup')
    def set_executable_opts(self):
        """Override executable options from Mixin class"""
        self.executable_opts = [
            f'-n{self.problem_size}', '-t1800',
        ]

    @run_after('setup')
    def set_output_file(self):
        """Set output file name"""
        self.output_file_patt = f'n{self.problem_size}*.*'
        self.output_file = sn.getitem(sn.glob(self.output_file_patt), 0)
        
    @run_after('setup')
    def set_mkl_env(self):
        """Source the env vars to get all necessary libraries on PATH"""
        self.prerun_cmds = ['. $MKLROOT/../../setvars.sh']

