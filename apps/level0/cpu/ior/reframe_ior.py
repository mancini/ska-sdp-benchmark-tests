# pylint: disable=C0301
"""
IOR benchmark
----------------------------

Context
~~~~~~~~~~~~~~~~~~~~~~~~

IOR is designed to measure parallel file system I/O performance through a variety of potential APIs.
This parallel program performs writes and reads to/from files and reports the resulting throughput
rates. The tests are configured in such a way to minimise the page caching effect on I/O bandwidth.
See `here <https://ior.readthedocs.io/en/latest/userDoc/tutorial.html#effect-of-page-cache-on-benchmarking>`__ for more details.

.. note::
  In order to run this test, an environment variable ``SCRATCH_DIR`` must be defined in the system
  partition with the path to the scratch directory of the platform. Otherwise the test will fail.

.. _test vars:

Test variables
~~~~~~~~~~~~~~~~~~~~~~~~

Several variable are defined in the tests which can be configured from the command line interface
(CLI). They are summarised as follows:

- ``num_nodes``: Number of nodes to run the test (default is 4)
- ``num_mpi_tasks_per_node``: Number of MPI processes per node (default is 8)
- ``block_size``: Block size of IOR test (default is 1g)
- ``transfer_size``: Transfer size of IOR test (default is 1m)
- ``num_segments``: Number of segments of IOR test (default is 1)

The variables ``block_size``, ``transfer_size`` and ``num_segments`` are IOR related. More details
on these variables can be found at `IOR documentation <https://ior.readthedocs.io/en/latest/intro.html>`_.

Any of these variables can be overridden from the CLI using ``-S`` option of ReFrame. The examples
are presented in :ref:`usage`.

Test parameterisation
~~~~~~~~~~~~~~~~~~~~~~~~

The test is parameterised with respect to two parameters namely I/O interface and file type. There
are 3 different I/O interfaces available

- ``posix``: POSIX I/O
- ``mpiio``: MPI I/O
- ``hdf5``: HDF5

We can write data to a single file or use file-per-process approach and tests are parameterised as
follows:

- ``single``: Single file for all processes
- ``fpp``: File per process

The parameterised tests can be controlled by tags which will be shown in the :ref:`usage` section.

.. _usage:

Usage
~~~~~~~~~~~~~~~~~~~~~~~~

The test can be run using following commands.

.. code-block:: bash

  cd ska-sdp-benchmark-tests
  conda activate ska-sdp-benchmark-tests
  reframe/bin/reframe -C reframe_config.py -c apps/level0/cpu/ior/reframe_ior.py --exec-policy=serial --run --performance-report

.. note::
  It is extremely important to use ``--exec-policy=serial`` for this particular test. By default,
  ReFrame executes the tests in `asynchronous mode <https://reframe-hpc.readthedocs.io/en/stable/pipeline.html>`_
  which means multiple jobs are executed at the same time if partition allows to do so. However,
  for this type of IO test, we do not want all the jobs using the underlying file system at the same
  time. So, we switch to serial execution where only one job at a time is executed on the partition.

To configure the test variables presented in :ref:`test vars` section we can use ``-S`` option as
follows:

.. code-block:: bash

  cd ska-sdp-benchmark-tests
  conda activate ska-sdp-benchmark-tests
  reframe/bin/reframe -C reframe_config.py -c apps/level0/cpu/ior/reframe_ior.py --exec-policy=serial --run --performance-report -S num_nodes=2

Multiple variables can be configured simple by repeating ``-S`` flag for each variable as follows:

.. code-block:: bash

  cd ska-sdp-benchmark-tests
  conda activate ska-sdp-benchmark-tests
  reframe/bin/reframe -C reframe_config.py -c apps/level0/cpu/ior/reframe_ior.py --exec-policy=serial --run --performance-report -S num_nodes=2 -S block_size=10g

By default all parameterised tests will be executed for a given partition. The list of tests can be
obtained using:

.. code-block:: bash

  cd ska-sdp-benchmark-tests
  conda activate ska-sdp-benchmark-tests
  reframe/bin/reframe -C reframe_config.py -c apps/level0/cpu/ior/reframe_ior.py -l

which will give following output :

.. code-block:: bash

  [ReFrame Setup]
  version:           3.9.0-dev.3+adca255d
  command:           'reframe/bin/reframe -C reframe_config.py -c apps/level0/cpu/ior/reframe_ior.py -l'
  launched by:       mahendra@alaska-login-0.novalocal
  working directory: '/home/mahendra/work/ska-sdp-benchmark-tests'
  settings file:     'reframe_config.py'
  check search path: (R) '/home/mahendra/work/ska-sdp-benchmark-tests/apps/level0/cpu/ior/reframe_ior.py'
  stage directory:   '/home/mahendra/work/ska-sdp-benchmark-tests/stage'
  output directory:  '/home/mahendra/work/ska-sdp-benchmark-tests/output'

  [List of matched checks]
  - IorTest_hdf5_single (found in '/home/mahendra/work/ska-sdp-benchmark-tests/apps/level0/cpu/ior/reframe_ior.py')
  - IorTest_posix_single (found in '/home/mahendra/work/ska-sdp-benchmark-tests/apps/level0/cpu/ior/reframe_ior.py')
  - IorTest_mpiio_single (found in '/home/mahendra/work/ska-sdp-benchmark-tests/apps/level0/cpu/ior/reframe_ior.py')
  - IorTest_posix_fpp (found in '/home/mahendra/work/ska-sdp-benchmark-tests/apps/level0/cpu/ior/reframe_ior.py')
  - IorTest_mpiio_fpp (found in '/home/mahendra/work/ska-sdp-benchmark-tests/apps/level0/cpu/ior/reframe_ior.py')
  - IorTest_hdf5_fpp (found in '/home/mahendra/work/ska-sdp-benchmark-tests/apps/level0/cpu/ior/reframe_ior.py')
  Found 6 check(s)

  Log file(s) saved in '/home/mahendra/work/ska-sdp-benchmark-tests/reframe.log', '/home/mahendra/work/ska-sdp-benchmark-tests/reframe.out'

As we can see from the output, ReFrame will execute all IO types and file type tests. In order to
choose only few parameterised tests, we can use ``-t`` flag to restrict the tests to given
parameters. For example, to run only POSIX IO interface and Single file variant

.. code-block:: bash

  cd ska-sdp-benchmark-tests
  conda activate ska-sdp-benchmark-tests
  reframe/bin/reframe -C reframe_config.py -c apps/level0/cpu/ior/reframe_ior.py --exec-policy=serial --run --performance-report -t posix$ -t single$

can be used. As in the case of ``-S`` option, ``-t`` can also be repeated as many times as user
want.

Test class documentation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
"""
# pylint: enable=C0301

import reframe as rfm
import reframe.utility.sanity as sn  # pylint: disable=import-error
import reframe.utility.osext as osext  # pylint: disable=import-error
from modules.base_class import RunOnlyBenchmarkBase

from modules.utils import filter_systems_by_env
from modules.reframe_extras import patch_launcher_command, MultiRunMixin
from reframe.core.builtins import variable, parameter, run_after, run_before, performance_function


# pylint: disable=C0116,E0401,E0602,E1101,W0201


@rfm.simple_test
class IorTest(RunOnlyBenchmarkBase, MultiRunMixin):
    """Main class of IOR read and write tests"""

    descr = 'IOR test to benchmark read and write performance of parallel file systems'

    # Variables to define number of nodes
    num_nodes = variable(int, value=4)
    # Variable to define number of mpi processes per node
    num_mpi_tasks_per_node = variable(int, value=8)
    # Variable to define block size
    block_size = variable(str, value='1g')
    # Variable to define transfer size
    transfer_size = variable(str, value='1m')
    # Variable to define number of segments
    num_segments = variable(int, value=1)

    # Parameterisation of tests
    # IO type: POSIX, MPIIO, HDF5
    io_type = parameter(['posix', 'mpiio', 'hdf5'])
    # File type: Single file, File per process (FPP)
    file_type = parameter(['single', 'fpp'])

    def __init__(self):
        super().__init__()
        self.valid_prog_environs = [
            'ior'
        ]
        self.valid_systems = filter_systems_by_env(self.valid_prog_environs)
        self.maintainers = [
            'Mahendra Paipuri (mahendra.paipuri@inria.fr)'
        ]
        self.exclusive_access = True
        self.time_limit = '0d2h00m0s'

    @run_after('init')
    def set_param_tags(self):
        """Add parameter tags to the test"""
        self.tags |= {
            f'io_type={self.io_type}',
            f'file_type={self.file_type}',
            f'block_size={self.block_size}',
            f'transfer_size={self.transfer_size}',
            f'num_segments={self.num_segments}',
            self.io_type,
            self.file_type,
        }

    @run_after('setup')
    def set_git_commit_tag(self):
        """Fetch git commit hash"""
        git_ref = osext.git_repo_hash(short=False, wd=self.prefix)
        self.tags |= {f'git={git_ref}'}

    @run_after('setup')
    def set_num_tasks_reservation(self):
        """Set number of tasks for job reservation"""
        # This method sets number of tasks for the reservation. Here ONLY resources are reserved
        if self.current_partition.scheduler.registered_name == 'oar':
            self.num_tasks_per_node = \
                (self.current_partition.processor.num_cpus
                 // self.current_partition.processor.num_cpus_per_core)
        else:
            self.num_tasks_per_node = self.current_partition.processor.num_cpus
        self.num_tasks = self.num_nodes * self.num_tasks_per_node

    @run_after('setup')
    def set_num_mpi_tasks(self):
        """Set number of MPI tasks"""
        self.num_mpi_tasks = self.num_nodes * self.num_mpi_tasks_per_node

    @run_after('setup')
    def set_tags(self):
        """Add tags to the test"""
        self.tags |= {
            f'num_nodes={self.num_nodes}',
            f'num_procs={self.num_mpi_tasks}',
        }

    @run_after('setup')
    def patch_job_launcher(self):
        """Monkey mock the job launcher command"""
        # Monkey mocking the actual job launcher command to remove -np/-n
        # argument.
        if self.job.launcher.registered_name in ['mpirun', 'mpiexec']:
            self.job.launcher.command = patch_launcher_command

    @run_after('setup')
    def set_executable(self):
        """Set executable name"""
        self.executable = 'ior'

    @run_after('setup')
    def set_executable_opts(self):
        """Set executable options"""
        self.executable_opts = [
            f'-a {self.io_type}', '-i 5', '-v', '-g', '-w', '-r', '-e',
            '-C', f'-b {self.block_size}', f'-t {self.transfer_size}',
            f'-s {self.num_segments}'
        ]
        if self.file_type == 'fpp':
            self.executable_opts += ['-F']

    @run_before('run')
    def export_env_vars(self):
        """Export env variables using OMPI_MCA param for OpenMPI"""
        # This is needed for g5k clusters as env vars are not exported to all nodes in
        # the reservation
        self.variables['OMPI_MCA_mca_base_env_list'] = \
            f'\"PATH;LD_LIBRARY_PATH;{";".join(self.variables.keys())}\"'

    @run_before('run')
    def chdir_to_scratch(self):
        """Add prerun command to change PWD to scratch dir before commencing test"""
        self.prerun_cmds = [
            'cd $SCRATCH_DIR'
        ]

    @run_before('run')
    def job_launcher_opts(self):
        """Set job launcher options"""
        self.job.launcher.options = [
            '--map-by node', f'-np {self.num_mpi_tasks}',
        ]

    @run_before('sanity')
    def set_sanity_patterns(self):
        """Set sanity patterns. Example stdout

        .. code-block:: text

            # Max Write: 940.74 MiB/sec (986.44 MB/sec)
            # Max Read:  1303.68 MiB/sec (1367.01 MB/sec)
            # Finished            : Mon Oct 18 10:52:25 2021

        """
        self.sanity_patterns = sn.all([
            sn.assert_found('Max Write: ', self.stdout),
            sn.assert_found('Max Read: ', self.stdout),
            sn.assert_found('Finished', self.stdout)
        ])

    @performance_function('MiB/s')
    def extract_write_bw(self):
        """Performance extraction function for extract write bandwidth. Sample stdout

        .. code-block:: text

            # Max Write: 940.74 MiB/sec (986.44 MB/sec)

        """
        return sn.extractsingle(
            r'Max Write:\s+(?P<write_bw>\S+) MiB/sec', self.stdout, 'write_bw', float
        )

    @performance_function('MiB/s')
    def extract_read_bw(self):
        """Performance extraction function for extract read bandwidth. Sample stdout

        .. code-block:: text

            # Max Read:  1303.68 MiB/sec (1367.01 MB/sec)

        """
        return sn.extractsingle(
            r'Max Read:\s+(?P<read_bw>\S+) MiB/sec', self.stdout, 'read_bw', float
        )

    @run_before('performance')
    def set_perf_patterns(self):
        """Set performance variables"""
        self.perf_variables = {
            'write_bw': self.extract_write_bw(),
            'read_bw': self.extract_read_bw(),
        }

    @run_before('performance')
    def set_reference_values(self):
        """Set reference perf variables"""
        self.reference = {
            '*': {
                'write_bw': (None, None, None, 'MiB/s'),
                'read_bw': (None, None, None, 'MiB/s'),
            }
        }
