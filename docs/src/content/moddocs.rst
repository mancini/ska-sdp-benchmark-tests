Documentation of modules
===================================

.. automodule:: modules.reframe_extras
  :members:

.. automodule:: modules.utils
  :members:
