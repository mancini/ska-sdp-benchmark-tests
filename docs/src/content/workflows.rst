Common Workflows
===================================

This section describes a number of widely used, common workflows that support a developer in using the benchmark suite.


1. Adding new tests
----------------------

To add a new test to the benchmark suite, follow the following steps:

1. Define whether the test belongs into level 0, level 1 or level 2. Then create a folder in the corresponding location and add the following files:

- ``reframe_<test_name>.py``: This is the main test file where we define the test class derived from the Benchmark base classes :code:`BenchmarkBase`, :code:`RunOnlyBenchmarkBase` or :code:`CompileOnlyBenchmarkBase`.
Make sure to use the one corresponding to your specific needs. Those classes are developed according to the ReFrame ideas documented in https://reframe-hpc.readthedocs.io/en/stable/tutorial_advanced.html#writing-a-run-only-regression-test.
- ``TEST_NAME.ipynb``: Jupyter notebook to plot the performance metrics derived from the test
- ``README.md``: A simple readme file that gives high level instructions on where to find the documentation of the test.



2. Define the test procedure:

    - Does the test need some sources or packages from the internet, be it its own sources, python packages or any other dependencies? If yes, create a test dependency that fetches everything. Every test or test dependency that accesses the internet needs to inherit from the ``UsesInternet`` mixin.

    .. literalinclude:: ../../../apps/level1/idg_test/reframe_idgtest.py
       :lines: 90-95

    - Does the test need to compile fetched dependencies? If yes, create a test dependency that builds the sources. If the sources are fetched in a previous test, be sure to include this as a dependent fixture: ``app_src = fixture(DownloadTest, scope='session')``. An example of such a build-dependency can be found in ``apps/level1/idg_test/reframe_idgtest.py``. The class ``IdgTestBuild`` is a build-test that is required by the actual IDG Test.

3. Write the test itself.

    - Define all dependencies as fixture, all parameters as `parameter` and all variables as `variable`. Tests are run for all permutations of parameters, whereas variables can define specific behaviour for a single run (like number of nodes).
    - Set the `valid_prog_environs` and the `valid_systems` in the `__init__` method. They define on which systems the test may be executed. `valid_systems` is mostly set by `filter_systems_by_env(self.valid_prog_environs)` while `valid_prog_environs` is usually a descriptor unique to your test. Please see the workflow `Adding new environment` for further details.
    - Define the executable and executable options. The executable is usually what your test effectively wants to test, like a pipeline-script or a compiled binary.
    - Define the Sanity Patterns. They are required and tell ReFrame whether the test execution was successful. In the sanity patterns you can define which patterns must and/or must not appear in the stdout and stderr. So for example if the application under test writes `Error` to the stderr in case an error happens, we can add the absence of this string as a sanity pattern. Or if we know that the test prints `Successfully executed ThisTest on 32 Nodes in 123.5 seconds` we can create a pattern that matches this expected result.

    .. literalinclude:: ../../../apps/level1/idg_test/reframe_idgtest.py
       :lines: 285-307

    - Define the Performance Functions. The performance functions extract any number of values from the output stream. So if the application under test for example writes timing measurements to the console, or bandwidth, throughput or any other metric that can be used to tell the performance, we can extract this number from the stdout or stderr using regular expressions. IDG for example does it like this:

    .. literalinclude:: ../../../apps/level1/idg_test/reframe_idgtest.py
       :lines: 309-363

    The sanity- and performance functions are both based on the concept of "Deferrable Functions". Be sure to check out the `official documentation <https://reframe-hpc.readthedocs.io/en/v4.6.1/deferrable_functions_reference.html>`__ on how to use them properly.

Those steps allow you to write a basic ReFrame test. For more in-detail view, take a look at the `ReFrame documentation <https://reframe-hpc.readthedocs.io/en/v4.6.1/>`__.
There is no strict convention on how to name the test. Already provided tests can be used as templates to write new tests.

2. Adding new system
----------------------------------------------

Every time we want to add a new system, typically we will need to follow these steps:

- Create a new python file ``<system_name>.py`` in ``config/systems`` folder.
- Add system configuration and define partitions for the system. More details on how to define a partition and naming conventions are presented later.
- Import this file into ``reframe_config.py`` and add this new system in the ``site_configuration``.
- The final step would be get the processor info using ``--detect-host-topology`` option on ReFrame of system nodes, place in the ``toplogies`` folder and include the file in ``processor`` key for each partition.

The user is advised to consult the `ReFrame documentation <https://reframe-hpc.readthedocs.io/en/stable/configure.html>`_ before doing so. The provided systems can be used as a template to add new systems.

We try to follow a certain convention in defining the system partition. Firstly, we define partitions, either physical or abstract, based on compiler toolchain and MPI implementation such that when we use this system, modules related to compiler and MPI will be loaded. Rest of the modules that are related to test will be added to the ``environs`` which will be discussed later. Consequently, we should also name these partitions in such a way that we can have a standard scheme. The benefit of having such a scheme is two-fold: able to get high level overview of partition quickly and by choosing an appropriate names, we can filter the systems for the tests easily. An example use case is that we want to run a certain test on all partitions that support GPUs. Using a partition name with ``gpu`` as suffix, we can simply filter all the partitions looking for a match with string ``gpu``.

We use the convention ``{prefix}-{compiler-name-major-ver}-{mpi-name-major-ver}-{interconnect-type}-{software-type}-{suffix}``.

- ``Prefix`` can be name of the partition or cluster.
- ``compiler-name-major-ver`` can be as follows:
   - ``gcc9``: GNU compiler toolchain with major version 9
   - ``icc20``: Intel compiler toolchain with major version 2020
   - ``xl16``: IBM XL toolchain with major version 16
   - ``aocc3``: AMD AOCC toolchain with major version 3
- ``mpi-name-major-ver`` is the name of the MPI implementation. Some of them are:
   - ``ompi4``: OpenMPI with major version 4
   - ``impi19``: Intel MPI with major version 2019
   - ``pmpi5``: IBM Spectrum MPI with major version 5
   - ``smpi10``: IBM Spectrum MPI with major version 10
- ``interconnect-type`` is type of interconnect on the partition.
   - ``ib``: Infiniband
   - ``rocm``: RoCE
   - ``opa``: Intel Omnipath
   - ``eth``: Ethernet TCP
- ``software-type`` is type of software stack used.
   - ``smod``: System provided software stack
   - ``umod``: User built software stack using Spack
- ``suffix`` can indicate any special properties of the partitions like gpu, high memory nodes, high priority job queues, *etc*. There can be multiple suffices each separated by a hyphen.

.. important::

  If the package uses calendar versioning, we use only last two digits of the year in the name to be concise. For example, Intel MPI 2019.* would be ``impi19``.

For instance, in the configuration shown in :ref:`content/tools:ReFrame configuration` ``compute-gcc9-ompi4-roce-umod`` tells us that the partition has GCC compiler with OpenMPI. It uses RoCE as interconnect and the softwares are built in user space using Spack.

.. important::

  It is recommended to stick to this convention and there can be more possibilities for each category which should be added as we add new systems.


3. Adding new environment
----------------------------------------------

Adding a new system is not enough to run the tests on this system. We need to tell our ReFrame tests that there is a new system available in the config. In order to minimise the redundancy in adding configuration details and avoid modifying the source code of the test, we choose to provide a ``environ`` for each test. For example, there is HPL test in ``apps/level0/hpl`` folder and for this test we define a environ in ``config/environs/hpl.py``.

.. note::

  System partitions and environments should have one-to-one mapping. It means, whatever environment we define within ``environs`` section in the system partition, we should put that partition within ``target_systems`` in each ``environ``.


All the modules that are needed to run the test, albeit compiler and MPI, will be added to the ``modules`` section in each ``environ``. For example, lets take a look at ``hpl.py`` file

.. literalinclude:: ../../../config/environs/hpl.py


There are two different environments namely ``intel-hpl`` and ``gnu-hpl``. As names suggests, ``intel-hpl`` uses HPL benchmark shipped out of MKL optimized for Intel chips. Whereas we use ``gnu-hpl`` for other chips like AMD using GNU toolchain. Notice that ``target_systems`` for ``intel-hpl`` has only partitions that have Intel MPI implementation (``impi`` in the name) whereas the ``gnu-hpl`` has ``target_systems`` have OpenMPI implementation. Within the test, we define only the ``valid program`` and find valid systems by filtering all systems that have the given environment defined for them.

For instance, we defined a new system partition that has Intel chip with name as ``mycluster-gcc-impi-ib-umod``. If we want HPL test to run on this system partition, we add ``intel-hpl`` to ``environs`` section in system partition and similarly add the name of this partition to ``target_systems`` in ``intel-hpl`` environment. Once we do that, the test will run on this partition without having to modify anything in the source code of the test.

If we want to add a new test, we will need to add new environment and following steps should be followed:

- Create a new file ``<env_name>.py`` in ``config/environs`` folder and add environment configuration for the tests. **It is important** that we add this new environment to existing and/or new system partitions that are defined in ``target_systems`` of the environment configuration.
- Finally, import ``<env_name>.py`` in the main ReFrame configuration file ``reframe_config.py`` and add it to the configuration dictionary.
