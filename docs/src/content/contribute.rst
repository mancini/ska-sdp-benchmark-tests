Contributing to the Benchmark Suite
=======================================

Common Modules
---------------------------
The common modules can be found in the directory `modules`. They consist of various implementations of things that are used by all benchmark tests.
For example, the specific test base classes are defined here, which are responsible for adding monitoring and tracing tools (See the `benchmon <https://gitlab.com/ska-telescope/sdp/ska-sdp-benchmark-monitor>`_ for further information) as well as providing hooks for pre- and postrun checks for benchmarks.

Unit Tests
---------------------------
Things that are not too much intertwined with ReFrame and therefore testable should where possible be tested by a UnitTest.
To write a unit test procedure you can take for example the test class for ``CondaEnvManager`` located in ``unittests/test_conda_env_manager.py``.

   - Create a file test with a class test for your benchmark :

     .. literalinclude:: ../../../unittests/test_conda_env_manager.py
       :lines: 1-11

   - Add a test method for each functionnality :

     .. literalinclude:: ../../../unittests/test_conda_env_manager.py
       :lines: 13-21

   - Check results via the ``self.assertTrue`` and ``self.assertFalse`` methods (`unittest doc <https://docs.python.org/3/library/unittest.html>`) :

     .. literalinclude:: ../../../unittests/test_conda_env_manager.py
       :lines: 19-20

   - Add the name of your unit test file (without the extension) in the list of unit tests to check in the ``sdp-unittest.sh`` script

     .. literalinclude:: ../../../unittests/test_conda_env_manager.py
       :lines: 19-20


We recommend to use the ``logging`` module systematically (`logging doc <https://docs.python.org/3/library/logging.html>`) which provides 6 custom level of screen outputs (e.g. ``CRITICAL``, ``ERROR``, ``WARNING``, ``INFO``, ``DEBUG``, ``NOSET``).

To perform our unit test procedure, we provide an experimental bash script ``sdp-unittest.sh`` to launch our different unit tests automatically with several available options. We use the package ``pytest`` (`pytest doc <https://docs.pytest.org/en/7.1.x/contents.html>`) to purpose a parallelized and efficient procedure for the unit test step.
