SDP BenchmarkSuite-Specific Use Cases
====================================================================

Adding Conda environment via CondaEnvManager
-----------------------------------------------

CondaEnvManager is a class to provide a specific conda environment for each test (benchmark) we want to create. The advantage is we remove the different problem of dependancies we could have with the ska-sdp-benchmark environment (e.g. a different version of numpy package). For the moment, we provide this approach only for the ``level0/cpu/fft`` test and for the associated unit test. We will use the unit test example as a basis below.

Create a conda environment
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
When you instantiate your class, you don't immediately create a conda environement with the name you provide.
This is just used within the CondaEnvManager to prepare everything. The environment is effectively created when you run ``.create()``

.. literalinclude:: ../../../unittests/test_conda_env_manager.py
   :lines: 25-26

Remove a conda environment
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Remove the created environment and all packages installed.

.. literalinclude:: ../../../unittests/test_conda_env_manager.py
   :lines: 18

Install a package via pip
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
The ``install_pip`` method provides a generic method which is able to download and install a package or install a package from a local folder if the package is already downloaded there.
You can also provide a ``requirements.txt`` file to directly install multiple packages (e.g. *Download and install a list of packages via pip locally*).

.. literalinclude:: ../../../unittests/test_conda_env_manager.py
   :lines: 58-61

Download locally a package via pip
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
You can download packages locally into a cache folder. This is useful if there is no access to internet available within the compute nodes on your cluster:

.. literalinclude:: ../../../unittests/test_conda_env_manager.py
   :lines: 66-73


Download and install locally a list of packages via pip
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
We can download a list of packages given via a ``requirements.txt`` file. We need to provide the directory of the requirements file and the installations steps :

.. literalinclude:: ../../../unittests/test_conda_env_manager.py
   :lines: 78-86

Purge the pip cache
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Remove packages in the cache folder for a created environment.

.. literalinclude:: ../../../unittests/test_conda_env_manager.py
   :lines: 87

Separation of Parts that Access the Internet
------------------------------------------------------------------
Any test or test dependency that accesses the internet needs to implement the mixin ``UsesInternet``. This mixin ensures that parts that need to
download assets from the internet are run on a partition where internet can be accessed. Any test that accesses the internet and does not
implement the ``UsesInternet`` mixin is **not guaranteed** to have internet available and might fail.

Best practice is to separate all dependencies that need to access the web into dependencies, be it dataset downloads, sources downloads or package
installations that fetch from repositories. Those dependencies must implement the ``UsesInternet`` mixin and should be defined as fixtures for the test.