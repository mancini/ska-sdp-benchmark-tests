# Build CI docker image

This folder contains Docker files and all auxiliary files that need to be put inside docker image. There are two sub folders `base/` and `cuda/` that contain Dockerfiles to build `base` and `cuda` tags, respectively. The docker image used in CI can be build with these files. We build these custom images not to repeat the same steps for every CI job. Everytime we change Dockerfile, we need to update the image and push it to registry. Before building copy `requirements.txt` and `docs/requirements.txt` into the `.ci/Dockerfiles/{base,cuda}`. For instance, to build `base` tag following commands needs to executed:

```
cd ska-sdp-benchmark-tests
cp requirements.txt .ci/Dockerfiles/base/requirements.txt
cp docs/requirements.txt .ci/Dockerfiles/base/requirements-tests.txt
cp .ci/Dockerfiles/update-perflogs.py .ci/Dockerfiles/base/update-perflogs.py
```

We use these files inside the docker image and all the files that needed to be copied in the docker image needs to be in the same root as `Dockerfile`. Once this is done, we can build image using

```
docker build -t registry.gitlab.com/ska-telescope/sdp/ska-sdp-benchmark-tests:base .
```

Similarly, to push the image into container registry, use

```
docker push registry.gitlab.com/ska-telescope/sdp/ska-sdp-benchmark-tests:base
```

Similar steps can be followed for `cuda` tag as well. **Note** that GPU tests are run on Kubernetes executor and by default, the `imagePullPolicy` of the runner is `if-not-present` and hence, if we update the image using the same tag, Kubernetes will not pull the latest changes in the image. If there are updates to the `cuda` tag, change the name of the tag and push the new tag to the registry. We need to update the name of the tag in `.gitlab-ci-tests.yml` file too.
