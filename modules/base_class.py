import os
from typing import Callable, Tuple
from shutil import which

import reframe as rfm
from reframe.core.builtins import run_before, run_after

from modules.utils import sdp_benchmark_tests_root


class BenchmarkBaseMixin(rfm.RegressionMixin):
    """Base mixin for all Regression Tests.
    Uses the :class:`rfm.RegressionMixin` as basis. This provides access to all ReFrame pipeline hooks.

    This mixin provides a list of prerun and a list of postrun checks.
    Checks can be added by the developer using `self.add_prerun_check(f)` and `self.add_postrun_check(f)` respectively.
    `f` needs to be a callable that requires no additional argument and returns either a `Tuple[bool, str]`
    or an exception. In the first case, the boolean reports on the predicate status (True if the check completed
    successfully, False otherwise). The string part is only used in case the predicate fails and reports
    to the user what exactly failed. If an exception is thrown while calling `f`, the predicate is defined to be
    `False` with the exception message being used as message string."""

    prerun_checks = None
    postrun_checks = None

    use_benchmon = False
    bm_hardware = False
    bm_software = False
    bm_run = False

    def __init__(self):
        """Initializes the pre- and postrun checks with an empty list.
        This is necessary because otherwise the list instances are shared across test instances."""
        super().__init__()
        self.prerun_checks = []
        self.postrun_checks = []

        self.use_benchmon = os.getenv('USE_BENCHMON', 'YES') == 'YES'
        if self.use_benchmon:
            self.bm_hardware = which('benchmon-hardware')
            self.bm_software = which('benchmon-software')
            self.bm_run = which('benchmon-run')

    def add_prerun_check(self, predicate: Callable[[], Tuple[bool, str]]):
        """Adds a prerun check to the prerun check list.

        :arg predicate: The predicate to be called. Returns either Tuple[bool, str] or an exception when called
        """
        self.prerun_checks.append(predicate)

    def add_postrun_checks(self, predicate: Callable[[], Tuple[bool, str]]):
        """Adds a postrun check to the postrun check list.

        :arg predicate: The predicate to be called. Returns either Tuple[bool, str] or an exception when called
        """
        self.postrun_checks.append(predicate)

    @run_after('setup')
    def run_prerun_checks(self):
        """Executes the list of prerun predicates. If any fail the test is skipped.
        Each failing test is reported to the user as a message using self.skip().
        """
        if self.prerun_checks is None:
            return

        msgs = []
        for i, pred in enumerate(self.prerun_checks):
            # run predicate
            try:
                res, msg = pred()
            except Exception as e:
                res = None
                msg = f"Exception: {e}"

            if not res:
                msgs.append(f"Check {i + 1} failed: {msg}")
        if len(msgs) > 0:
            nl = "\n>> "
            self.skip(f"{len(msgs)}/{len(self.prerun_checks)} prerun checks failed. Messages:\n>> {nl.join(msgs)}")

    @run_before('run')
    def add_benchmon_cmds(self):
        if not self.use_benchmon:
            return

        out_dir = f"{sdp_benchmark_tests_root()}/perflogs/benchmon/{self.current_system.name}/{self.current_partition.name}/{self.current_environ.name}/{self.name}/{self.job.jobid}"

        # add static benchmon code
        if self.bm_hardware is not None:
            self.prerun_cmds.append(f'benchmon-hardware --save-dir {out_dir} --no-long-checks')
        if self.bm_software is not None:
            self.prerun_cmds.append(f'benchmon-software --save-dir {out_dir}')

        if self.bm_run is not None:
            bm_run_cmd = f"benchmon-run --save-dir {out_dir} --system-sampling-interval 30"
            if hasattr(self, 'num_nodes') and self.num_nodes > 1:
                # use multi node
                self.prerun_cmds.append("NODES=$(echo $(scontrol show hostname $SLURM_JOB_NODELIST) | tr -s ' ' ',')")
                self.prerun_cmds.append(f"srun --overlap -l -m arbitrary -w $NODES {bm_run_cmd} &")
            else:
                self.prerun_cmds.append(f"{bm_run_cmd} &")

            self.prerun_cmds.append("BENCHMON_PID=$!")
            self.prerun_cmds.append("sleep 5")

            self.postrun_cmds += [
                "sleep 5",
                "kill -15 $BENCHMON_PID"
            ]

    @run_before('cleanup')
    def run_postrun_checks(self):
        """Executes the list of postrun predicates. If any fail the test is marked as failed.
        Each failing test is reported to the user as a message using a RuntimeError.
        """
        if self.postrun_checks is None:
            return

        msgs = []
        for i, pred in enumerate(self.postrun_checks):
            # run predicate
            try:
                res, msg = pred()
            except Exception as e:
                res = None
                msg = f"Exception: {e}"

            if not res:
                msgs.append(f"Check {i + 1} failed: {msg}")
        if len(msgs) > 0:
            nl = "\n>> "
            raise RuntimeError(f"{len(msgs)}/{len(self.postrun_checks)} postrun checks failed. "
                               f"Marking test as failed. "
                               f"Messages:\n>> {nl.join(msgs)}")


class RunOnlyBenchmarkBase(rfm.RunOnlyRegressionTest, BenchmarkBaseMixin):
    """Base class for RunOnly Regression Tests.
    Uses the :class:`BenchmarkBaseMixin` to mix into the :class:`rfm.RunOnlyRegressionTest` class.
    This is used if a test only needs to run an application without needing to compile anything before."""
    def __init__(self):
        """__init__ needs to call the Mixin-Init explicitly, otherwise it does not get called at all."""
        BenchmarkBaseMixin.__init__(self)
        super().__init__()


class CompileOnlyBenchmarkBase(rfm.CompileOnlyRegressionTest, BenchmarkBaseMixin):
    """Base class for CompileOnly Regression Tests.
    Uses the :class:`BenchmarkBaseMixin` to mix into the :class:`rfm.CompileOnlyRegressionTest` class.
    This is used if a test only needs to compile sources without the usual run phases."""
    def __init__(self):
        """__init__ needs to call the Mixin-Init explicitly, otherwise it does not get called at all."""
        BenchmarkBaseMixin.__init__(self)
        super().__init__()


class BenchmarkBase(rfm.RegressionTest, BenchmarkBaseMixin):
    """Base class for full Regression Tests.
    Uses the :class:`BenchmarkBaseMixin` to mix into the :class:`rfm.RegressionTest` class.
    This is used if a test needs to compile sources and then execute an usual run regression test."""
    def __init__(self):
        """__init__ needs to call the Mixin-Init explicitly, otherwise it does not get called at all."""
        BenchmarkBaseMixin.__init__(self)
        super().__init__()

    @run_after('setup')
    def add_launcher_name(self):
        self.launcher_name = self.job.launcher.registered_name
